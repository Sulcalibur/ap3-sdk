#!/bin/sh
SDK_FOLDER=$1
DEPLOY_FOLDER=target/deploy
INSTALLER_FOLDER=target/installer
mkdir -p $DEPLOY_FOLDER
cd $DEPLOY_FOLDER
git clone git@bitbucket.org:atlassian/ap3-sdk-bin.git ./ap3-sdk

cd ./ap3-sdk
git checkout develop

rm -rf ./*

git add -u .
git status
git commit -m "cleaning up sdk"

cd ../../../
echo "currently in: " `pwd`
cp -R $INSTALLER_FOLDER/$SDK_FOLDER/* $DEPLOY_FOLDER/ap3-sdk/
cd $DEPLOY_FOLDER/ap3-sdk

git add ./
git commit -m "updating sdk installer"
git push origin develop

