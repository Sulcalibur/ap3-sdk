#!/bin/sh

has_required_version()
{
    if [ -z "$JAVA7_EXISTS" ] ; then
        if [ `expr $2 \>= $1` -lt 1 ] ; then
            # version is less than required
            echo "0"
        else
            JAVA7_EXISTS="true"
            echo "1"
        fi
    else
        echo "1"
    fi
}

get_mac_java7()
{
    echo `ls -d /Library/Java/JavaVirtualMachines/*1.7.*/ 2>/dev/null | head -n 1`
}

get_java_home_version()
{
    TMP_VERSION=`eval $JAVA_HOME/bin/java -version 2>&1`       
    TMP_VERSION=`expr "$TMP_VERSION" : '.*"\(1.[0-9\.]*\)["_]'`
    echo $TMP_VERSION
}

REQUIRED_JAVA_VERSION=1.7.0
REQUIRED_GIT_VERSION=1.6.5

####################################################
# Determine the location of JAVA_HOME              #
# This was modified from the mvn command licensed  #
# under the Apache-2.0 license                     #
# http://www.apache.org/licenses/LICENSE-2.0       #
####################################################
# OS specific support.  $var _must_ be set to either true or false.
cygwin=false;
darwin=false;
mingw=false
case "`uname`" in
  CYGWIN*) cygwin=true ;;
  MINGW*) mingw=true;;
  Darwin*) darwin=true 
           if [ -n "$JAVA_HOME" ] ; then
             JAVA_VERSION=`get_java_home_version`       
             HAS_JAVA7=`has_required_version "$REQUIRED_JAVA_VERSION" "$JAVA_VERSION"`
             if [ $HAS_JAVA7 -gt 0 ] ; then
                JAVACMD="$JAVA_HOME/bin/java"
             else
                MAC_JAVA7_HOME=`get_mac_java7`
             fi
           else
            MAC_JAVA7_HOME=`get_mac_java7`
           fi
           
           if [ -n "$MAC_JAVA7_HOME" ] ; then
            JAVA_HOME="$MAC_JAVA7_HOME/Contents/Home"
           fi
           ;;
esac

if [ -z "$JAVA_HOME" ] ; then
  if [ -r /etc/gentoo-release ] ; then
    JAVA_HOME=`java-config --jre-home`
  fi
fi

# For Cygwin, ensure paths are in UNIX format before anything is touched
if $cygwin ; then
  [ -n "$JAVA_HOME" ] &&
    JAVA_HOME=`cygpath --unix "$JAVA_HOME"`
  [ -n "$CLASSPATH" ] &&
    CLASSPATH=`cygpath --path --unix "$CLASSPATH"`
fi

# For Migwn, ensure paths are in UNIX format before anything is touched
if $mingw ; then
  [ -n "$JAVA_HOME" ] &&
    JAVA_HOME="`(cd "$JAVA_HOME"; pwd)`"
fi

if [ -n $JAVA_HOME ] ; then
    JAVA_VERSION=`get_java_home_version`
    HAS_JAVA7=`has_required_version "$REQUIRED_JAVA_VERSION" "$JAVA_VERSION"`
    
    if [ $HAS_JAVA7 -lt 1 ] ; then
        unset JAVA_HOME
    fi
fi

if [ -z "$JAVA_HOME" ]; then
  javaExecutable="`which javac`"
  if [ -n "$javaExecutable" -a ! "`expr \"$javaExecutable\" : '\([^ ]*\)'`" = "no" ]; then
    # readlink(1) is not available as standard on Solaris 10.
    readLink=`which readlink`
    if [ ! `expr "$readLink" : '\([^ ]*\)'` = "no" ]; then
      javaExecutable="`readlink -f \"$javaExecutable\"`"
      javaHome="`dirname \"$javaExecutable\"`"
      javaHome=`expr "$javaHome" : '\(.*\)/bin'`
      JAVA_HOME="$javaHome"
      export JAVA_HOME
    fi
  fi
fi

if [ -z "$JAVACMD" ] ; then
  if [ -n "$JAVA_HOME"  ] ; then
    if [ -x "$JAVA_HOME/jre/sh/java" ] ; then
      # IBM's JDK on AIX uses strange locations for the executables
      JAVACMD="$JAVA_HOME/jre/sh/java"
    else
      JAVACMD="$JAVA_HOME/bin/java"
    fi
  else
    JAVACMD="`which java`"
  fi
fi

if [ ! -x "$JAVACMD" ] ; then
  echo "Error: JAVA_HOME is not defined correctly."
  echo "  We cannot execute $JAVACMD"
  exit 1
fi

if [ -z "$JAVA_HOME" ] ; then
  echo "Warning: JAVA_HOME environment variable is not set."
fi

# For Cygwin, switch paths to Windows format before running java
if $cygwin; then
  [ -n "$JAVA_HOME" ] &&
    JAVA_HOME=`cygpath --path --windows "$JAVA_HOME"`
  [ -n "$CLASSPATH" ] &&
    CLASSPATH=`cygpath --path --windows "$CLASSPATH"`
fi

#################################################
# Determine the if we have required jdk version #
#################################################
JDK_VERSION=`eval $JAVACMD -version 2>&1`       
JDK_VERSION=`expr "$JDK_VERSION" : '.*"\(1.[0-9\.]*\)["_]'`
HAS_JAVA7=`has_required_version "$REQUIRED_JAVA_VERSION" "$JDK_VERSION"`

if [ ! $HAS_JAVA7 -gt 0 ] ; then
    echo "JAVA CMD version $REQUIRED_JAVA_VERSION is required but found version $JDK_VERSION, aborting..."
    if $darwin ; then
        echo "You can install java 7 for use with the AP3 SDK by simply running the oracle JDK installer from:"
        echo "http://www.oracle.com/technetwork/java/javase/downloads/index.html"
    else
        echo "Ubuntu users can install java 7 by doing:"
        echo "sudo add-apt-repository ppa:webupd8team/java"
        echo "sudo apt-get update"
        echo "sudo apt-get install oracle-java7-installer"
    fi
    exit 1;
fi

if [ -z "$JAVA_HOME" ] ; then
  JAVA_CMD=`command -v java`
  if [ ! -z "$JAVA_CMD" ] ; then
    javaHome="`dirname \"$JAVA_CMD\"`"
    javaHome=`expr "$javaHome" : '\(.*\)/bin'`
    JAVA_HOME="$javaHome"
  fi
fi

if [ -z "$JAVA_HOME" ] ; then
    echo "JAVA is required but was not found, aborting..."
    if $darwin ; then
        echo "You can install java 7 for use with the AP3 SDK by simply running the oracle JDK installer from:"
        echo "http://www.oracle.com/technetwork/java/javase/downloads/index.html"
    else
        echo "Ubuntu users can install java 7 by doing:"
        echo "sudo add-apt-repository ppa:webupd8team/java"
        echo "sudo apt-get update"
        echo "sudo apt-get install oracle-java7-installer"
    fi
    exit 1;
fi

JAVA_VERSION=`eval $JAVA_HOME/bin/java -version 2>&1`       
JAVA_VERSION=`expr "$JAVA_VERSION" : '.*"\(1.[0-9\.]*\)["_]'`

HAS_JAVA7=`has_required_version "$REQUIRED_JAVA_VERSION" "$JAVA_VERSION"`
if [ ! $HAS_JAVA7 ] ; then
    echo "JAVA_HOME version $REQUIRED_JAVA_VERSION is required but found version $JDK_VERSION, aborting..."
    if $darwin ; then
        echo "You can install java 7 for use with the AP3 SDK by simply running the oracle JDK installer from:"
        echo "http://www.oracle.com/technetwork/java/javase/downloads/index.html"
    else
        echo "Ubuntu users can install java 7 by doing:"
        echo "sudo add-apt-repository ppa:webupd8team/java"
        echo "sudo apt-get update"
        echo "sudo apt-get install oracle-java7-installer"
    fi
    exit 1;
fi

#################################################
# Determine the if we have required git version #
#################################################
GIT_CMD=`command -v git`
if [ -z "$GIT_CMD" ] ; then
  echo "GIT is required but was not found, aborting..."
  exit 1;
fi

GIT_VERSION=`eval $GIT_CMD --version 2>&1`    
GIT_VERSION=`$GIT_VERSION | cut -d ' ' -f 3`

HAS_GIT=`has_required_version "$REQUIRED_GIT_VERSION" "$GIT_VERSION"`
if [ ! $HAS_GIT ] ; then
    echo "GIT version $REQUIRED_GIT_VERSION is required but found version $GIT_VERSION, aborting..."
    exit 1;
fi
