#!/bin/sh

AP3_HOME=${HOME}/.ap3-sdk
rm -rf ${AP3_HOME}
git clone https://bitbucket.org/atlassian/ap3-sdk-bin.git ${AP3_HOME}

BINDIR=${AP3_HOME}/bin

chmod +x ${BINDIR}/ap3

case "`uname`" in
  Darwin*) darwin=true 
           rm /usr/local/bin/ap3 2>/dev/null
           mkdir -p /usr/local/bin 2>/dev/null
           ln -s ${BINDIR}/ap3 /usr/local/bin/ap3
           ;;
  *)
           echo "we need to make symlinks which require sudo access..."
           sudo rm /usr/local/bin/ap3 2>/dev/null
           sudo mkdir -p /usr/local/bin 2>/dev/null
           sudo ln -s ${BINDIR}/ap3 /usr/local/bin/ap3
           ;;
esac



. ${AP3_HOME}/ap3-setup-env.sh
. ${AP3_HOME}/ap3-setup-config.sh

BASHIT_ALIASES=${HOME}/.bash_it/aliases
BASHIT_COMPLETE=${HOME}/.bash_it/completion

if [ -x "$BASHIT_ALIASES" ] ; then
    echo "found bash-it, copying aliases..."
    rm -f $BASHIT_ALIASES/enabled/ap3.aliases.bash
    rm -f $BASHIT_ALIASES/available/ap3.aliases.bash
    cp $AP3_HOME/ap3.aliases.bash $BASHIT_ALIASES/available/
    chmod +x $BASHIT_ALIASES/available/ap3.aliases.bash
    ln -s $BASHIT_ALIASES/available/ap3.aliases.bash $BASHIT_ALIASES/enabled/ap3.aliases.bash
    BASHIT_ENABLED=true
fi

if [ -x "$BASHIT_COMPLETE" ] ; then
    echo "found bash-it, copying completions..."
    rm -f $BASHIT_COMPLETE/enabled/ap3.completion.bash
    rm -f $BASHIT_COMPLETE/available/ap3.completion.bash
    cp $AP3_HOME/ap3.completion.bash $BASHIT_COMPLETE/available/
    chmod +x $BASHIT_COMPLETE/available/ap3.completion.bash
    ln -s $BASHIT_COMPLETE/available/ap3.completion.bash $BASHIT_COMPLETE/enabled/ap3.completion.bash
    BASHIT_ENABLED=true
fi

echo "AP3 setup complete!"
echo "run 'ap3' or 'ap3 help' to see what's possible"
if [ $BASHIT_ENABLED ];then
    echo "bash-it plugins installed. restart your shell and type 'ap3-help' to see aliases"
fi
${BINDIR}/ap3 help