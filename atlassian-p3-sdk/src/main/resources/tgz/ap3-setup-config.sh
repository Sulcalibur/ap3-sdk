#!/bin/sh
PRG="${0}"
while [ -h "${PRG}" ] ; do
  ls=`ls -ld "${PRG}"`
  link=`expr "${ls}" : '.*-> \(.*\)$'`
  if expr "${link}" : '/.*' > /dev/null; then
    PRG="${link}"
  else
    PRG=`dirname "${PRG}"`/"${link}"
  fi
done
PRGDIR=`dirname "${PRG}"`
MYDIR=`cd "${PRGDIR}" && pwd -P`

AP3_CONFIG_DIR=`cd ~ && pwd -P`/.ap3
rm -rf $AP3_CONFIG_DIR
mkdir -p $AP3_CONFIG_DIR

NOW=`date +%Y%m%d`
UPDATE_FILE="${AP3_CONFIG_DIR}/.last_update"
touch ${UPDATE_FILE}
echo "${NOW}" > ${UPDATE_FILE};

git clone --recursive https://bitbucket.org/atlassian/ap3-sdk-templates.git $AP3_CONFIG_DIR/templates

