package com.atlassian.ap3.spi.command;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.inject.Inject;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.config.Ap3ConfigurationManager;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.spi.config.Ap3ConfigurationCommand;

import org.slf4j.Logger;

import io.airlift.command.Group;
import io.airlift.command.Option;
import io.airlift.command.OptionType;

@Group(name = "config", description = "Configures the Atlassian Plugins 3 SDK", defaultCommand = ConfigDefaultCommand.class)
public abstract class AbstractConfigCommand<T> extends BaseAp3Command implements Ap3ConfigurationCommand<T>
{
    @Option(name = {"-g", "--global"}, title = "Global Configuration", description = "Global configuration vs. Project configuration", type = OptionType.GROUP)
    private boolean global = false;

    @Inject
    private Ap3ConfigurationManager configurationManager;

    @Inject
    private Prompter prompter;
    
    protected boolean isGlobal()
    {
        return global;
    }

    @Override
    public void run() throws Ap3Exception
    {
        Path projectFile = Paths.get("").resolve(Ap3ConfigurationManager.PROJECT_CONFIG_FILENAME);
        
        if(!global && Files.notExists(projectFile))
        {
            try
            {
                prompter.showError("Current directory is not a valid AP3 project.");
                prompter.showWarning("Either re-run this command with the -g|--global flag, or run: ap3 init");
                return;
            }
            catch (PrompterException e)
            {
                //do nothing
            }
        }


        try
        {
            save(getUpdatedConfigurationEntry());
        }
        catch (IOException e)
        {
            throw new Ap3Exception("unable to save configuration",e);
        }
    }

    protected abstract T getUpdatedConfigurationEntry() throws Ap3Exception; 
    
    public Class<T> getEntryType()
    {
        ParameterizedType myType = (ParameterizedType)getClass().getGenericSuperclass();
        return (Class<T>) myType.getActualTypeArguments()[0];
    }

    public void save(Object configEntry) throws IOException
    {
        if(global)
        {
            configurationManager.saveGlobalEntry(configEntry);
        }
        else
        {
            configurationManager.saveProjectEntry(configEntry,Paths.get(""));
        }
    }

    @Override
    protected Logger getLog()
    {
        return super.getLog();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public <T> T getCurrentConfigurationEntry(Class<T> type)
    {
        if(global)
        {
            return configurationManager.getGlobalConfigurationEntry(type);
        }
        else
        {
            return configurationManager.getProjectConfigurationEntry(type,Paths.get(""));
        }
        
    }
}
