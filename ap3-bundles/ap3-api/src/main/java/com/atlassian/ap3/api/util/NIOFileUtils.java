package com.atlassian.ap3.api.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @since version
 */
public class NIOFileUtils
{
    public static String readFileToString(Path fileToRead) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        String newLine = System.getProperty("line.separator");
        
        try(BufferedReader reader = Files.newBufferedReader(fileToRead, Charset.defaultCharset()))
        {
            String line = null;
            while ((line = reader.readLine()) != null)
            {
                sb.append(line).append(newLine);
            }
            
            return sb.toString();
        }
    }
    
    public static void cleanDirectory(final Path baseDir) throws IOException
    {
        Files.walkFileTree(baseDir,new SimpleFileVisitor<Path>(){
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
            {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException
            {
                if(null != exc)
                {
                    throw exc;
                }

                if(!baseDir.getFileName().equals(dir.getFileName()))
                {
                    Files.delete(dir);
                }
                return FileVisitResult.CONTINUE;
            }
        });
    }

    public static void writeStringToFile(Path fileToWrite, String content) throws IOException
    {
        try(BufferedWriter writer = Files.newBufferedWriter(fileToWrite, Charset.defaultCharset()))
        {
            writer.write(content,0,content.length());
        }
    }
    
    public static boolean directoryIsEmpty(Path dir) throws IOException
    {
        if(Files.isDirectory(dir))
        {
            try(DirectoryStream<Path> ds = Files.newDirectoryStream(dir))
            {
                if(ds.iterator().hasNext())
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        
        return false;
    }
}
