package com.atlassian.ap3.api.prompt;

import com.atlassian.ap3.api.Ap3Exception;

public class PrompterException extends Ap3Exception
{
    public PrompterException(String message)
    {
        super(message);
    }

    public PrompterException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
