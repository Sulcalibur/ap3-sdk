package com.atlassian.ap3.spi.command;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.cli.Ap3Cli;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

import org.eclipse.sisu.Nullable;

import io.airlift.command.Command;
import io.airlift.command.Group;
import io.airlift.command.model.CommandGroupMetadata;
import io.airlift.command.model.CommandMetadata;

@Named
@Group(name = "start", description = "Starts an application container", defaultCommand = StartDefaultCommand.class)
@Command(name = "default", hidden = true)
public class StartDefaultCommand extends BaseAp3Command
{
    @Inject
    @Nullable
    private CommandGroupMetadata groupMetadata;

    private final Prompter prompter;
    private final Ap3Cli cli;

    @Inject
    public StartDefaultCommand(Prompter prompter, Ap3Cli cli)
    {
        this.prompter = prompter;
        this.cli = cli;
    }


    @Override
    public void run() throws Ap3Exception
    {
        if(null != groupMetadata)
        {
            CommandMetadata metadata = promptForApp(groupMetadata.getCommands());
            Ap3Command command = cli.getParsedCommand(metadata.getType());

            if(null != command)
            {
                command.run();
            }
        }
    }

    private CommandMetadata promptForApp(List<CommandMetadata> commands) throws Ap3Exception
    {
        CommandMetadata command = null;

        Collection<CommandMetadata> choices = Collections2.filter(commands, new Predicate<CommandMetadata>()
        {
            @Override
            public boolean apply(CommandMetadata input)
            {
                return !input.isHidden();
            }
        });

        try
        {
            command = prompter.promptForChoice("What would you like to start?", choices, new Function<CommandMetadata, String>() {
                @Override
                public String apply(CommandMetadata input)
                {
                    return "[" + input.getName() + "] " + input.getDescription();
                }
            });
        }
        catch (PrompterException e)
        {
            throw new Ap3Exception("Error prompting for app", e);
        }

        return command;
    }
}
