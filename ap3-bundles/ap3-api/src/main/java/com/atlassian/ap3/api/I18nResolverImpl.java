package com.atlassian.ap3.api;

import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.util.*;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.BundleDelegatingClassloader;

import com.google.common.collect.Iterators;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import static com.google.common.base.Preconditions.checkNotNull;

@Singleton
@Named
public class I18nResolverImpl implements I18nResolver
{
    private final BundleContext bundleContext;
    private final List<Bundle> i18nBundleCache;

    @Inject
    public I18nResolverImpl(BundleContext bundleContext)
    {
        this.bundleContext = bundleContext;
        this.i18nBundleCache = new ArrayList<Bundle>();
    }

    @Override
    public String getText(String key, Object... arguments)
    {
        return getText(Locale.getDefault(), key, arguments);
    }

    @Override
    public String getText(Locale locale, String key, Object... arguments)
    {
        String message = null;

        for (Bundle plugin : getI18nBundles())
        {
            try
            {
                ResourceBundle bundle = getBundle(locale, plugin);
                if (bundle.containsKey(key))
                {
                    message = bundle.getString(key);
                    message = MessageFormat.format(message, (Object[]) arguments);
                    break;
                }
            }
            catch (MissingResourceException e)
            {
                //Ignore
            }
        }
        return message;
    }

    protected ResourceBundle getBundle(Locale locale, Bundle plugin)
    {
        return ResourceBundle.getBundle("i18n", locale, new BundleDelegatingClassloader(plugin));
    }

    public List<Bundle> getI18nBundles()
    {
        if (i18nBundleCache.isEmpty())
        {
            for (Bundle bundle : bundleContext.getBundles())
            {
                if (null != bundle.getResource("i18n.properties"))
                {
                    i18nBundleCache.add(bundle);
                }
            }
        }

        return i18nBundleCache;
    }

}
