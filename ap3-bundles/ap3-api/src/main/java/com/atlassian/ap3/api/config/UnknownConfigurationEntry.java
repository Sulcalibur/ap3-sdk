package com.atlassian.ap3.api.config;

import com.google.gson.JsonElement;

/**
 * @since version
 */
public class UnknownConfigurationEntry
{
    private final String entryLabel;
    private final JsonElement jsonElement;
    
    public UnknownConfigurationEntry(String entryLabel, JsonElement jsonElement)
    {
        this.entryLabel = entryLabel;
        this.jsonElement = jsonElement;
    }

    public String getEntryLabel()
    {
        return entryLabel;
    }

    public JsonElement getJsonElement()
    {
        return jsonElement;
    }
}
