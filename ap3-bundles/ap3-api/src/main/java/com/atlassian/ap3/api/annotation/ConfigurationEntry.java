package com.atlassian.ap3.api.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.inject.Qualifier;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Qualifier
@Target(TYPE)
@Retention(RUNTIME)
@Documented
public @interface ConfigurationEntry
{
    String value();
}
