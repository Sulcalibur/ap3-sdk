package com.atlassian.ap3;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.Enumeration;

import com.google.common.collect.Iterators;

import org.osgi.framework.Bundle;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since version
 */
public class BundleDelegatingClassloader extends ClassLoader
{
    private final Bundle bundle;

    public BundleDelegatingClassloader(final Bundle bundle)
    {
        super(null);
        checkNotNull(bundle);
        this.bundle = bundle;
    }

    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException
    {
        return bundle.loadClass(name);
    }

    @Override
    public URL getResource(String name)
    {
        return bundle.getResource(name);
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException
    {
        return findResources(name);
    }

    @Override
    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException
    {
        return bundle.loadClass(name);
    }

    @Override
    public Class<?> findClass(final String name) throws ClassNotFoundException
    {
        return bundle.loadClass(name);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Enumeration<URL> findResources(final String name) throws IOException
    {
        Enumeration<URL> e = bundle.getResources(name);

        if (e == null)
        {
            e = Iterators.asEnumeration(Iterators.<URL>emptyIterator());
        }
        else
        {
            // For some reason, getResources() sometimes returns nothing, yet getResource() will return one.  This code
            // handles that strange case
            if (!e.hasMoreElements())
            {
                final URL resource = findResource(name);
                if (resource != null)
                {
                    e = Iterators.asEnumeration(Arrays.asList(resource).iterator());
                }
            }
        }
        return e;
    }

    @Override
    public URL findResource(final String name)
    {
        URL url = bundle.getResource(name);

        return url;
    }
}

