package com.atlassian.ap3.api.oauth;

public class OAuthKeyPair
{
    private final String privateKey;
    private final String publicKey;

    public OAuthKeyPair(String privateKey, String publicKey)
    {
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    public String getPrivateKey()
    {
        return privateKey;
    }

    public String getPublicKey()
    {
        return publicKey;
    }
}
