package com.atlassian.ap3.api.prompt;

import java.io.IOException;
import java.util.*;

import javax.inject.Inject;
import javax.inject.Singleton;

import com.atlassian.ap3.api.I18nResolver;

import com.google.common.base.Function;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import jline.ANSIBuffer;
import jline.ConsoleReader;

@Singleton
public class PrettyPrompter implements Prompter
{
    private I18nResolver i18n;

    private ConsoleReader consoleReader;

    private boolean useAnsiColor;

    @Inject
    public PrettyPrompter(I18nResolver i18n)
    {
        this.i18n = i18n;

        try
        {
            this.consoleReader = new ConsoleReader();
        }
        catch (IOException e)
        {
            //ignore, as this should never happen
        }

        String mavencolor = System.getenv("MAVEN_COLOR");
        String ap3color = System.getenv("AP3_COLOR");
        if (StringUtils.isNotBlank(mavencolor))
        {
            useAnsiColor = Boolean.parseBoolean(mavencolor);
        }
        else if (StringUtils.isNotBlank(ap3color))
        {
            useAnsiColor = Boolean.parseBoolean(ap3color);
        }
        else
        {
            useAnsiColor = false;
        }
    }

    @Override
    public String prompt(String message)
            throws PrompterException
    {

        try
        {
            return consoleReader.readLine(formatMessage(message, null, null));
        }
        catch (IOException e)
        {
            throw new PrompterException("Failed to read user response", e);
        }
    }

    @Override
    public String prompt(String message, String defaultReply)
            throws PrompterException
    {

        try
        {
            String line = consoleReader.readLine(formatMessage(message, null, defaultReply));

            if (StringUtils.isEmpty(line))
            {
                line = defaultReply;
            }

            return line;
        }
        catch (IOException e)
        {
            throw new PrompterException("Failed to read user response", e);
        }
    }

    @Override
    public String prompt(String message, List possibleValues, String defaultReply)
            throws PrompterException
    {
        String formattedMessage = formatMessage(message, possibleValues, defaultReply);

        String line;

        do
        {
//            try
//            {
//                writePrompt(formattedMessage);
//            } catch (IOException e)
//            {
//                throw new PrompterException("Failed to present prompt", e);
//            }

            try
            {
                line = consoleReader.readLine(formattedMessage);
            }
            catch (IOException e)
            {
                throw new PrompterException("Failed to read user response", e);
            }

            if (StringUtils.isEmpty(line))
            {
                line = defaultReply;
            }

            if (line != null && !listContainsNoCase(possibleValues, line))
            {
                try
                {
                    String invalid = "Invalid selection.";
                    if (useAnsiColor)
                    {
                        ANSIBuffer ansiBuffer = new ANSIBuffer();
                        ansiBuffer.append(ANSIBuffer.ANSICodes
                                                    .attrib(FG_RED))
                                  .append(ANSIBuffer.ANSICodes
                                                    .attrib(BOLD))
                                  .append("Invalid selection.")
                                  .append(ANSIBuffer.ANSICodes
                                                    .attrib(OFF));
                        invalid = ansiBuffer.toString();
                    }
                    consoleReader.printString(invalid);
                    consoleReader.printNewline();
                }
                catch (IOException e)
                {
                    throw new PrompterException("Failed to present feedback", e);
                }
            }
        }
        while (line == null || !listContainsNoCase(possibleValues, line));

        return line;
    }

    @Override
    public <T> T promptForChoice(String message, Iterable<T> choices, Function<T, String> choiceDisplay) throws PrompterException
    {
        Pair<String, Map<String, T>> messageAndAnswers = null;
        String formattedMessage = "";
        Map<String, T> answerMap = new HashMap<>();

        String line = null;

        if (useAnsiColor)
        {
            messageAndAnswers = formatAnsiChoiceMessage(message, choices, choiceDisplay);
        }
        else
        {
            messageAndAnswers = formatPlainChoiceMessage(message, choices, choiceDisplay);
        }

        formattedMessage = messageAndAnswers.getLeft();
        answerMap = messageAndAnswers.getRight();

        do
        {
//            try
//            {
//                writePrompt(formattedMessage);
//            } catch (IOException e)
//            {
//                throw new PrompterException("Failed to present prompt", e);
//            }

            try
            {
                consoleReader.printString(formattedMessage);
                line = consoleReader.readLine(boldIfPossible("Choose a number: "));
            }
            catch (IOException e)
            {
                throw new PrompterException("Failed to read user response", e);
            }

            if (line != null && !answerMap.keySet().contains(line))
            {
                try
                {
                    String invalid = "Invalid selection.";
                    if (useAnsiColor)
                    {
                        ANSIBuffer ansiBuffer = new ANSIBuffer();
                        ansiBuffer.append(ANSIBuffer.ANSICodes
                                                    .attrib(FG_RED))
                                  .append(ANSIBuffer.ANSICodes
                                                    .attrib(BOLD))
                                  .append("Invalid selection.")
                                  .append(ANSIBuffer.ANSICodes
                                                    .attrib(OFF));
                        invalid = ansiBuffer.toString();
                    }
                    consoleReader.printString(invalid);
                    consoleReader.printNewline();
                }
                catch (IOException e)
                {
                    throw new PrompterException("Failed to present feedback", e);
                }
            }
        }
        while (line == null || !answerMap.keySet().contains(line));

        return answerMap.get(line);

    }

    @Override
    public <T> T promptForChoice(String message, Iterable<T> choices) throws PrompterException
    {
        return promptForChoice(message, choices, new Function<T, String>()
        {
            @Override
            public String apply(T input)
            {
                return input.toString();
            }
        });

    }

    private <T> Pair<String, Map<String, T>> formatAnsiChoiceMessage(String message, Iterable<T> choices, Function<T, String> choiceDisplay) throws PrompterException
    {
        ANSIBuffer query = new ANSIBuffer();
        query.bold(message + ":\n");

        List<String> answers = new ArrayList<>();
        Map<String, T> answerValueMap = new HashMap<>();

        int counter = 1;

        for (T choice : choices)
        {
            String answer = String.valueOf(counter);
            query.bold(answer);
            if (counter < 10)
            {
                query.append(":  ");
            }
            else
            {
                query.append(": ");
            }

            query.append(choiceDisplay.apply(choice)).append("\n");

            answers.add(answer);
            answerValueMap.put(answer, choice);

            counter++;
        }

        return Pair.of(query.toString(), answerValueMap);
    }

    private <T> Pair<String, Map<String, T>> formatPlainChoiceMessage(String message, Iterable<T> choices, Function<T, String> choiceDisplay) throws PrompterException
    {
        StringBuffer query = new StringBuffer();
        query.append(message).append(":\n");

        List<String> answers = new ArrayList<>();
        Map<String, T> answerValueMap = new HashMap<>();

        int counter = 1;

        for (T choice : choices)
        {
            String answer = String.valueOf(counter);
            query.append(answer);
            if (counter < 10)
            {
                query.append(":  ");
            }
            else
            {
                query.append(": ");
            }

            query.append(choiceDisplay.apply(choice)).append("\n");

            answers.add(answer);
            answerValueMap.put(answer, choice);

            counter++;
        }

        return Pair.of(query.toString(), answerValueMap);
    }

    private String boldIfPossible(String message)
    {
        ANSIBuffer query = new ANSIBuffer();
        query.setAnsiEnabled(useAnsiColor);
        query.bold(message);
        return query.toString();
    }

    @Override
    public String prompt(String message, List possibleValues)
            throws PrompterException
    {
        return prompt(message, possibleValues, null);
    }

    @Override
    public String promptForPassword(String message)
            throws PrompterException
    {

        try
        {
            return consoleReader.readLine(message, new Character('*'));
        }
        catch (IOException e)
        {
            throw new PrompterException("Failed to read user response", e);
        }
    }

    @Override
    public String promptNotBlank(String message) throws PrompterException
    {
        return promptNotBlank(message, null);
    }

    @Override
    public String promptNotBlank(String message, String defaultValue) throws PrompterException
    {
        String value;
        if (StringUtils.isBlank(defaultValue))
        {
            value = prompt(requiredMessage(message));
        }
        else
        {
            value = prompt(message, defaultValue);
        }

        if (StringUtils.isBlank(value))
        {
            value = promptNotBlank(message, defaultValue);
        }
        return value;
    }

    @Override
    public boolean promptForBoolean(String message) throws PrompterException
    {
        return promptForBoolean(message, null);
    }

    @Override
    public boolean promptForBoolean(String message, String defaultValue) throws PrompterException
    {
        String answer;
        boolean bool;
        if (StringUtils.isBlank(defaultValue))
        {
            answer = prompt(requiredMessage(message), YN_ANSWERS);
        }
        else
        {
            answer = prompt(message, YN_ANSWERS, defaultValue);
        }

        if ("y".equals(answer.toLowerCase()))
        {
            bool = true;
        }
        else
        {
            bool = false;
        }

        return bool;
    }

    @Override
    public int promptForInt(String message, int defaultInt) throws PrompterException
    {
        String userVal = promptNotBlank(message, Integer.toString(defaultInt));
        int userInt;
        if (!StringUtils.isNumeric(userVal))
        {
            userInt = promptForInt(message, defaultInt);
        }
        else
        {
            userInt = Integer.parseInt(userVal);
        }
        return userInt;
    }

    @Override
    public String i18nPrompt(String key, Object... args) throws PrompterException
    {
        return prompt(i18n.getText(key, args));
    }

    @Override
    public String i18nPrompt(String key, String defaultReply, Object... args) throws PrompterException
    {
        return prompt(i18n.getText(key, args), defaultReply);
    }

    @Override
    public String i18nPrompt(String key, List possibleValues, Object... args) throws PrompterException
    {
        return prompt(i18n.getText(key, args), possibleValues);
    }

    @Override
    public String i18nPrompt(String key, List possibleValues, String defaultReply, Object... args) throws PrompterException
    {
        return prompt(i18n.getText(key, args), possibleValues, defaultReply);
    }

    @Override
    public String i18nPromptForPassword(String key, Object... args) throws PrompterException
    {
        return promptForPassword(i18n.getText(key, args));
    }

    @Override
    public void i18nShowMessage(String key, Object... args) throws PrompterException
    {
        showMessage(i18n.getText(key, args));
    }

    @Override
    public String i18nPromptNotBlank(String key, Object... args) throws PrompterException
    {
        return promptNotBlank(i18n.getText(key, args));
    }

    @Override
    public String i18nPromptNotBlank(String key, String defaultValue, Object... args) throws PrompterException
    {
        return promptNotBlank(i18n.getText(key, args), defaultValue);
    }

    @Override
    public boolean i18nPromptForBoolean(String key, Object... args) throws PrompterException
    {
        return promptForBoolean(i18n.getText(key, args));
    }

    @Override
    public boolean i18nPromptForBoolean(String key, String defaultValue, Object... args) throws PrompterException
    {
        return promptForBoolean(i18n.getText(key, args), defaultValue);
    }

    @Override
    public int i18nPromptForInt(String key, int defaultInt, Object... args) throws PrompterException
    {
        return promptForInt(i18n.getText(key, args), defaultInt);
    }

    protected boolean listContainsNoCase(List<String> possibleValues, String input)
    {
        for (String value : possibleValues)
        {
            if (value.equalsIgnoreCase(input))
            {
                return true;
            }
        }

        return false;
    }

    protected String requiredMessage(String message)
    {
        String formattedMessage = message;
        if (useAnsiColor)
        {
            ANSIBuffer ansiBuffer = new ANSIBuffer();
            ansiBuffer.append(ANSIBuffer.ANSICodes
                                        .attrib(PrettyPrompter.BOLD))
                      .append(ANSIBuffer.ANSICodes
                                        .attrib(PrettyPrompter.FG_RED))
                      .append(message)
                      .append(ANSIBuffer.ANSICodes
                                        .attrib(PrettyPrompter.OFF));
            formattedMessage = ansiBuffer.toString();
        }

        return formattedMessage;
    }

    protected String formatMessage(String message, List possibleValues, String defaultReply)
    {
        if (useAnsiColor)
        {
            return formatAnsiMessage(message, possibleValues, defaultReply);
        }
        else
        {
            return formatPlainMessage(message, possibleValues, defaultReply);
        }
    }

    private String formatAnsiMessage(String message, List possibleValues, String defaultReply)
    {
        ANSIBuffer formatted = new ANSIBuffer();

        formatted.append(message);

        if (possibleValues != null && !possibleValues.isEmpty())
        {
            formatted.append(" (");

            for (Iterator it = possibleValues.iterator(); it.hasNext(); )
            {
                String possibleValue = (String) it.next();

                formatted.attrib(possibleValue, BOLD);

                if (it.hasNext())
                {
                    formatted.append("/");
                }
            }

            formatted.append(")");
        }

        if (defaultReply != null)
        {
            formatted.append(ANSIBuffer.ANSICodes
                                       .attrib(FG_GREEN))
                     .append(ANSIBuffer.ANSICodes
                                       .attrib(BOLD))
                     .append(" [")
                     .append(defaultReply)
                     .append("]")
                     .append(ANSIBuffer.ANSICodes
                                       .attrib(OFF));
        }

        formatted.append(": ");
        return formatted.toString();
    }

    private String formatPlainMessage(String message, List possibleValues, String defaultReply)
    {
        StringBuffer formatted = new StringBuffer(message.length() * 2);

        formatted.append(message);

        if (possibleValues != null && !possibleValues.isEmpty())
        {
            formatted.append(" (");

            for (Iterator it = possibleValues.iterator(); it.hasNext(); )
            {
                String possibleValue = (String) it.next();

                formatted.append(possibleValue);

                if (it.hasNext())
                {
                    formatted.append('/');
                }
            }

            formatted.append(')');
        }

        if (defaultReply != null)
        {
            formatted.append(" [")
                     .append(defaultReply)
                     .append("]");
        }

        formatted.append(": ");
        return formatted.toString();
    }

    public void showMessage(String message)
            throws PrompterException
    {
        try
        {
            consoleReader.printString(message);
            consoleReader.printNewline();
            consoleReader.flushConsole();
        }
        catch (IOException e)
        {
            throw new PrompterException("Failed to present prompt", e);
        }

    }

    @Override
    public void showError(String message) throws PrompterException
    {
        String formattedMessage = message;
        
        if (useAnsiColor)
        {
            ANSIBuffer ansiBuffer = new ANSIBuffer();
            ansiBuffer.append(ANSIBuffer.ANSICodes
                                        .attrib(PrettyPrompter.BOLD))
                      .append(ANSIBuffer.ANSICodes
                                        .attrib(PrettyPrompter.FG_RED))
                      .append(message)
                      .append(ANSIBuffer.ANSICodes
                                        .attrib(PrettyPrompter.OFF));
            formattedMessage = ansiBuffer.toString();
        }
        
        showMessage(formattedMessage);
    }

    @Override
    public void showInfo(String message) throws PrompterException
    {
        String formattedMessage = message;

        if (useAnsiColor)
        {
            ANSIBuffer ansiBuffer = new ANSIBuffer();
            ansiBuffer.append(ANSIBuffer.ANSICodes
                                        .attrib(PrettyPrompter.BOLD))
                      .append(ANSIBuffer.ANSICodes
                                        .attrib(PrettyPrompter.FG_GREEN))
                      .append(message)
                      .append(ANSIBuffer.ANSICodes
                                        .attrib(PrettyPrompter.OFF));
            formattedMessage = ansiBuffer.toString();
        }

        showMessage(formattedMessage);
    }

    @Override
    public void showWarning(String message) throws PrompterException
    {
        String formattedMessage = message;

        if (useAnsiColor)
        {
            ANSIBuffer ansiBuffer = new ANSIBuffer();
            ansiBuffer.append(ANSIBuffer.ANSICodes
                                        .attrib(PrettyPrompter.BOLD))
                      .append(ANSIBuffer.ANSICodes
                                        .attrib(PrettyPrompter.FG_YELLOW))
                      .append(message)
                      .append(ANSIBuffer.ANSICodes
                                        .attrib(PrettyPrompter.OFF));
            formattedMessage = ansiBuffer.toString();
        }

        showMessage(formattedMessage);
    }

    @Override
    public void replaceMessage(String message) throws PrompterException
    {
        try
        {
            consoleReader.setCursorPosition(0);
            consoleReader.killLine();
            consoleReader.putString(message);
            consoleReader.flushConsole();
        }
        catch (IOException e)
        {
            throw new PrompterException("Failed to present prompt", e);
        }
    }


}
