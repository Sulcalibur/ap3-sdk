package com.atlassian.ap3.cli;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.command.Ap3CommandManager;
import com.atlassian.ap3.spi.command.Ap3Command;

import com.google.inject.Inject;

import io.airlift.command.Cli;
import io.airlift.command.ParseArgumentsUnexpectedException;
import io.airlift.command.ParseCommandUnrecognizedException;

/**
 * @since version
 */
public class Ap3CliImpl implements Ap3Cli
{
    private final Ap3CommandManager commandManager;

    private final Ap3CommandFactory commandFactory;
    
    private Cli<Ap3Command> ap3;
    
    private String[] originalArgs;
    
    @Inject
    public Ap3CliImpl(Ap3CommandManager commandManager, Ap3CommandFactory commandFactory)
    {
        this.commandManager = commandManager;
        this.commandFactory = commandFactory;
    }

    public Ap3CliImpl() {
        this.commandManager = null;
        this.commandFactory = null;
    }
    
    @Override
    public void run(String[] args)
    {
        originalArgs = args;
        
        //printBanner();
        printTinyBanner();

        ap3 = Cli.buildCli("ap3", Ap3Command.class)
                .withDefaultCommand(Ap3Help.class)
                .withCommand(Ap3Help.class)
                .withCommands(commandManager.getCommandClasses())
                .withDescription("An SDK to build Atlassian Plugins")
                .build();

        try
        {
            Ap3Command commandToRun = ap3.parse(commandFactory,args);
            commandToRun.run();
        }
        catch (ParseCommandUnrecognizedException | ParseArgumentsUnexpectedException urce)
        {
            System.err.println(urce.getLocalizedMessage());
            try
            {
                ap3.parse(commandFactory,"help").run();
            }
            catch (Ap3Exception e)
            {
                e.printStackTrace();
            }
        }
            
        catch (Ap3Exception e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Override
    public Ap3Command getParsedCommand(String name)
    {
        Ap3Command cmd = commandManager.getCommandByName(name);
        
        return ap3.parse(cmd, originalArgs);
    }

    @Override
    public Ap3Command getParsedCommand(Class<?> type)
    {
        Ap3Command cmd = commandManager.getCommandByType(type);
        return ap3.parse(cmd, originalArgs);
    }

    private void printBanner()
    {
        System.out.println();
        System.out.println("      .\"`\".              \n" +
                "  .-./ _=_ \\.-.         \n" +
                " {  (,(oYo),) }}    ___  _   _               _                ___  ______ _____ \n" +
                " {{ |   \"   |} }   / _ \\| | | |             (_)              / _ \\ | ___ \\____ | \n" +
                " { { \\(---)/  }}  / /_\\ \\ |_| | __ _ ___ ___ _  __ _ _ __   / /_\\ \\| |_/ /   / /  \n" +
                " {{  }'-=-'{ } }  |  _  | __| |/ _` / __/ __| |/ _` | '_ \\  |  _  ||  __/    \\ \\ \n" +
                " { { }._:_.{  }}  | | | | |_| | (_| \\__ \\__ \\ | (_| | | | | | | | || |   .___/ /\n" +
                " {{  } -:- { } }  \\_| |_/\\__|_|\\__,_|___/___/_|\\__,_|_| |_| \\_| |_/\\_|   \\____/\n" +
                " {_{ }`===`{  _}      \n" +
                "((((\\)     (/))))");
    }
    
    private void printTinyBanner()
    {
        System.out.println();
        System.out.println("  .-. \n" +
                "o(o_o)o AP3 SDK\n" +
                "( --- ) by Atlassian\n" +
                "--------------------");
    }
}
