package com.atlassian.ap3.ext.gson;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

import com.atlassian.ap3.api.annotation.ConfigurationEntry;
import com.atlassian.ap3.api.config.UnknownConfigurationEntry;

import com.google.gson.*;
import com.google.gson.internal.JsonReaderInternalAccess;
import com.google.gson.internal.Streams;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

/**
 * @since version
 */
public class Ap3ConfigEntryObjectAdapterFactory implements TypeAdapterFactory
{
    private final Set<Class<?>> entryTypes;

    public Ap3ConfigEntryObjectAdapterFactory()
    {
        this.entryTypes = new HashSet<>();
    }


    @Override
    public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type)
    {
        Type mapType = type.getType();
        Class<?> rawType = type.getRawType();

        if (!Map.class.isAssignableFrom(rawType) || !(mapType instanceof ParameterizedType))
        {
            return null;
        }
        ParameterizedType mapParameterizedType = (ParameterizedType) mapType;
        Type[] keyAndValueTypes = mapParameterizedType.getActualTypeArguments();

        if (keyAndValueTypes.length < 2 || !keyAndValueTypes[0].equals(String.class) || !keyAndValueTypes[1].equals(Object.class))
        {
            return null;
        }

        final Map<String, TypeAdapter> labelToDelegate = new LinkedHashMap<>();
        final Map<Class<?>, TypeAdapter> subtypeToDelegate = new LinkedHashMap<>();

        for (Class<?> entryType : entryTypes) {

            String label = entryType.getSimpleName();

            if(entryType.isAnnotationPresent(ConfigurationEntry.class))
            {
                label = entryType.getAnnotation(ConfigurationEntry.class).value();
            }

            TypeAdapter delegate = gson.getDelegateAdapter(this, TypeToken.get(entryType));

            labelToDelegate.put(label,delegate);
            subtypeToDelegate.put(entryType, delegate);
        }

        return (TypeAdapter<T>) new Ap3ConfigurationEntryMapAdapter(labelToDelegate,subtypeToDelegate);
    }

    public void registerConfigurationTypes(Set<Class<?>> types)
    {

        entryTypes.addAll(types);
    }

    public void registerConfigurationType(Class<?> type)
    {
        entryTypes.add(type);
    }

    private class Ap3ConfigurationEntryMapAdapter extends TypeAdapter<Map<String, Object>>
    {
        private final Map<String, TypeAdapter> labelToDelegate;
        private final Map<Class<?>, TypeAdapter> subtypeToDelegate;

        private Ap3ConfigurationEntryMapAdapter(Map<String, TypeAdapter> labelToDelegate, Map<Class<?>, TypeAdapter> subtypeToDelegate)
        {
            this.labelToDelegate = labelToDelegate;
            this.subtypeToDelegate = subtypeToDelegate;
        }

        @Override
        public void write(JsonWriter out, Map<String, Object> configMap) throws IOException
        {
            out.beginObject();

            for(Map.Entry<String,Object> entry : configMap.entrySet())
            {
                String label = entry.getKey();
                out.name(label);
                Object configEntry = entry.getValue();
                Class<?> srcType = configEntry.getClass();

                TypeAdapter delegate = subtypeToDelegate.get(srcType);

                if (delegate == null) {
                    throw new JsonParseException("cannot serialize " + srcType.getName() + "; did you register the entry class?");
                }

                delegate.write(out,entry.getValue());
            }

            out.endObject();
            System.out.println("");
        }

        @Override
        public Map<String, Object> read(JsonReader in) throws IOException
        {
            JsonToken peek = in.peek();
            if (peek == JsonToken.NULL)
            {
                in.nextNull();
                return null;
            }
            Map<String, Object> newMap = new HashMap<>();

            in.beginObject();
            while (in.hasNext())
            {
                JsonReaderInternalAccess.INSTANCE.promoteNameToValue(in);

                JsonElement jsonElement = Streams.parse(in);
                //String key = Streams.parse(in).getAsString();

                String label = jsonElement.getAsString();

                TypeAdapter delegate = labelToDelegate.get(label);

                if(null == delegate)
                {
                    JsonElement jsonObject = Streams.parse(in);
                    UnknownConfigurationEntry unknownEntry = new UnknownConfigurationEntry(label,jsonObject);
                    newMap.put(label, unknownEntry);
                }
                else
                {
                    Object value = delegate.read(in);
                    newMap.put(label, value);
                }
            }
            in.endObject();

            return newMap;
        }
    }
}
