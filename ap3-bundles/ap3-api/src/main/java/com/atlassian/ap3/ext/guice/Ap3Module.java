package com.atlassian.ap3.ext.guice;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import com.atlassian.ap3.api.I18nResolver;
import com.atlassian.ap3.api.I18nResolverImpl;
import com.atlassian.ap3.api.command.Ap3CommandManager;
import com.atlassian.ap3.api.command.Ap3CommandManagerImpl;
import com.atlassian.ap3.cli.Ap3Cli;
import com.atlassian.ap3.cli.Ap3CliImpl;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.matcher.Matchers;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;

import org.apache.maven.model.building.DefaultModelBuilder;
import org.apache.maven.model.building.ModelBuilder;
import org.eclipse.aether.impl.AetherModule;
import org.ops4j.peaberry.Peaberry;

import static org.ops4j.peaberry.Peaberry.service;
import static org.ops4j.peaberry.util.TypeLiterals.export;

@Named
public class Ap3Module extends AbstractModule
{
    
    @Override
    protected void configure()
    {
        install(Peaberry.osgiModule());

        bindListener(Matchers.any(), new AbstractMethodTypeListener(PostConstruct.class)
        {
            @Override
            protected <I> void hear(final Method method, TypeEncounter<I> encounter)
            {
                encounter.register(new InjectionListener<I>()
                {
                    public void afterInjection(I injectee)
                    {
                        try
                        {
                            method.invoke(injectee);
                        }
                        catch (IllegalAccessException e)
                        {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        catch (InvocationTargetException e)
                        {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                });
            }
        });
        
        bind( export( Ap3CommandManager.class ) ).toProvider(service(Ap3CommandManagerImpl.class).export()).asEagerSingleton();
        bind( export( I18nResolver.class ) ).toProvider(service(I18nResolverImpl.class).export()).asEagerSingleton();
        bind( export( Ap3Cli.class ) ).toProvider(service(Ap3CliImpl.class).export()).asEagerSingleton();

        bind(Ap3Cli.class).toProvider(service(Ap3Cli.class).single());
        
        bind( ModelBuilder.class ).to(DefaultModelBuilder.class).in(Scopes.SINGLETON);
        install(new AetherModule());
       
        
    }
}
