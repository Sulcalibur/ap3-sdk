package com.atlassian.ap3.api.kit;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.jar.Attributes;

import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.util.NamingUtil;
import com.atlassian.ap3.api.util.ZipFileSystemUtil;
import com.atlassian.plugin.remotable.descriptor.DescriptorAccessor;
import com.atlassian.plugin.remotable.descriptor.PolyglotDescriptorAccessor;

import org.apache.commons.lang3.StringUtils;
import org.osgi.framework.Constants;

import static com.google.common.collect.Sets.newHashSet;

public final class BundlePackageBuilder
{
    private static final DateFormat BUILD_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    private static final String REMOTE_PLUGIN_KEY_SEPARATOR = "----remoteplugin-";
    private static final String ATLASSIAN_PLUGIN_KEY = "Atlassian-Plugin-Key";
    private static final String SPRING_CONTEXT_KEY = "Spring-Context";
    
    private final Path projectRoot;
    private final Set<String> excludePaths;
    private final Set<Path> bundledJars;
    private final Map<String,String> manifestEntries;
    private final String pluginKey;
    private final String pluginVersion;

    public BundlePackageBuilder(Path projectRoot, String... pathsToExclude)
    {
        this.projectRoot = projectRoot;
        this.excludePaths = newHashSet(pathsToExclude);
        this.bundledJars = newHashSet();

        PolyglotDescriptorAccessor descriptorAccessor = new PolyglotDescriptorAccessor(projectRoot.toAbsolutePath().toFile());
        
        this.pluginKey = getPluginKey(descriptorAccessor);
        this.pluginVersion = getPluginVersion(descriptorAccessor);
        
        this.manifestEntries = defaultManifest(pluginKey,pluginVersion);
    }
    
    public BundlePackageBuilder addJar(Path jarFile)
    {
        bundledJars.add(jarFile);
        
        return this;
    }

    public BundlePackageBuilder addManifestEntry(String key, String value)
    {
        manifestEntries.put(key, value);

        return this;
    }

    public Path build() throws IOException
    {
        Path tmpJar = NamingUtil.tempFilename(pluginKey + REMOTE_PLUGIN_KEY_SEPARATOR, ".jar");
        
        try(FileSystem pluginJar = ZipFileSystemUtil.createZipFileSystem(tmpJar))
        {
            addProjectFiles(projectRoot,excludePaths,pluginJar);

            if (Files.notExists(projectRoot.resolve("META-INF/MANIFEST.MF")))
            {
                addManifest(manifestEntries,pluginJar);
            }

            if(!bundledJars.isEmpty())
            {
                for(Path jar : bundledJars)
                {
                    addJarContents(jar,pluginJar);
                }
            }
        }
        
        if (Files.size(tmpJar) < 1)
        {
            return null;
        }
        return tmpJar;
    }

    private void addJarContents(Path jar, final FileSystem pluginJar) throws IOException
    {
        try(FileSystem srcJar = ZipFileSystemUtil.openZipFileSystem(jar))
        {
            Files.walkFileTree(srcJar.getPath("/"),new SimpleFileVisitor<Path>(){

                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException
                {
                    Files.createDirectories(pluginJar.getPath(dir.toString()));
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
                {
                    Path dest = pluginJar.getPath(file.toString());
                    if(Files.notExists(dest))
                    {
                        Files.copy(file,dest);
                    }
                    return FileVisitResult.CONTINUE;
                }
            });
        }
 
    }

    private void addProjectFiles(Path projectRoot, Set<String> excludePaths, FileSystem zipFile) throws IOException
    {
        Files.walkFileTree(projectRoot,new ProjectVisitor(projectRoot,excludePaths,zipFile));
    }

    private void addManifest(Map<String, String> entries, FileSystem zipFile) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : entries.entrySet())
        {
            sb.append(entry.getKey()).append(": ").append(entry.getValue()).append('\n');
        }
        sb.append('\n');

        Path metainf = Files.createDirectories(zipFile.getPath("/META-INF"));
        Files.write(metainf.resolve("MANIFEST.MF"),sb.toString().getBytes(),StandardOpenOption.CREATE);
    }

    private Map<String,String> defaultManifest(String key, String version)
    {
        Map<String,String> mf = new HashMap<>();
        
        final String buildDateStr = String.valueOf(BUILD_DATE_FORMAT.format(new Date()));

        mf.put(Attributes.Name.MANIFEST_VERSION.toString(),"1.0");
        mf.put(Constants.BUNDLE_SYMBOLICNAME,key);
        mf.put(Constants.BUNDLE_VERSION,version);
        mf.put(Constants.BUNDLE_MANIFESTVERSION,"2");
        mf.put(ATLASSIAN_PLUGIN_KEY,key);
        mf.put(SPRING_CONTEXT_KEY,"*;timeout:=60");
        mf.put(Constants.DYNAMICIMPORT_PACKAGE,"*");
        mf.put(Constants.IMPORT_PACKAGE,"org.springframework.beans.factory, com.atlassian.plugin.osgi.bridge.external, org.jruby.ext.posix");
        
        return mf;
    }

    private String getPluginKey(DescriptorAccessor descriptorAccessor)
    {
        return descriptorAccessor.getDescriptor().getRootElement().attributeValue("key");
    }

    private String getPluginVersion(DescriptorAccessor descriptorAccessor)
    {
        return descriptorAccessor.getDescriptor().getRootElement().element("plugin-info").element("version").getTextTrim();
    }

    private class ProjectVisitor extends SimpleFileVisitor<Path>
    {
        private final Path projectRoot;
        private final Set<String> excludes;
        private final FileSystem zipFile;
        
        private ProjectVisitor(Path projectRoot, Set<String> excludes, FileSystem zipFile)
        {
            this.projectRoot = projectRoot;
            this.excludes = excludes;
            this.zipFile = zipFile;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException
        {
            String dirname = dir.getFileName().toString();
            String relativePath = projectRoot.relativize(dir).toString();

            if ((dirname.length() > 1 && StringUtils.startsWith(dirname, ".")) || excludes.contains(relativePath))
            {
                return FileVisitResult.SKIP_SUBTREE;
            }
            Files.createDirectories(zipFile.getPath("/",relativePath));
            
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
        {
            String relativePath = projectRoot.relativize(file).toString();
            String filename = file.getFileName().toString();
            
            if(excludes.contains(relativePath))
            {
                return FileVisitResult.CONTINUE;    
            }
            
            if (attrs.isRegularFile() && !".ap3".equals(filename) && !".gitignore".equals(filename))
            {
                Files.copy(file,zipFile.getPath("/",relativePath),StandardCopyOption.REPLACE_EXISTING);
            }

            return FileVisitResult.CONTINUE;
        }
    }
}
