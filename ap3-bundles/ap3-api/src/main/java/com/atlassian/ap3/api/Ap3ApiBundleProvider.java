package com.atlassian.ap3.api;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

@Named
@Singleton
public class Ap3ApiBundleProvider
{
    private final BundleContext bundleContext;

    @Inject
    public Ap3ApiBundleProvider(BundleContext bundleContext) 
    {
        this.bundleContext = bundleContext;
    }
    
    public Bundle getApiBundle()
    {
        return bundleContext.getBundle();
    }
}
