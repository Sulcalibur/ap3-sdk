package com.atlassian.ap3.api.prompt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.common.base.Function;
import com.google.inject.ImplementedBy;

@ImplementedBy(PrettyPrompter.class)
public interface Prompter
{
    public static final List<String> YN_ANSWERS = new ArrayList<String>(Arrays.asList("Y", "N"));
    public static final int OFF = 0;
    public static final int BOLD = 1;
    public static final int UNDERSCORE = 4;
    public static final int BLINK = 5;
    public static final int REVERSE = 7;
    public static final int CONCEALED = 8;
    public static final int FG_BLACK = 30;
    public static final int FG_RED = 31;
    public static final int FG_GREEN = 32;
    public static final int FG_YELLOW = 33;
    public static final int FG_BLUE = 34;
    public static final int FG_MAGENTA = 35;
    public static final int FG_CYAN = 36;
    public static final int FG_WHITE = 37;
    public static final char ESC = 27;
    
    String prompt(String message) throws PrompterException;

    String prompt(String message, String defaultReply) throws PrompterException;

    String prompt(String message, List possibleValues) throws PrompterException;

    String prompt(String message, List possibleValues, String defaultReply) throws PrompterException;

    String promptForPassword(String message) throws PrompterException;
    
    <T> T promptForChoice(String message,Iterable<T> choices) throws PrompterException;

    void showMessage(String message) throws PrompterException;
    void showError(String message) throws PrompterException;
    void showInfo(String message) throws PrompterException;
    void showWarning(String message) throws PrompterException;
    void replaceMessage(String message) throws PrompterException;

    String promptNotBlank(String message) throws PrompterException;

    String promptNotBlank(String message, String defaultValue) throws PrompterException;

    boolean promptForBoolean(String message) throws PrompterException;

    boolean promptForBoolean(String message, String defaultValue) throws PrompterException;

    int promptForInt(String message, int defaultInt) throws PrompterException;

    String i18nPrompt(String key, Object... args) throws PrompterException;

    String i18nPrompt(String key, String defaultReply, Object... args) throws PrompterException;

    String i18nPrompt(String key, List possibleValues, Object... args) throws PrompterException;

    String i18nPrompt(String key, List possibleValues, String defaultReply, Object... args) throws PrompterException;

    String i18nPromptForPassword(String key, Object... args) throws PrompterException;

    void i18nShowMessage(String key, Object... args) throws PrompterException;

    String i18nPromptNotBlank(String key, Object... args) throws PrompterException;

    String i18nPromptNotBlank(String key, String defaultValue, Object... args) throws PrompterException;

    boolean i18nPromptForBoolean(String key, Object... args) throws PrompterException;

    boolean i18nPromptForBoolean(String key, String defaultValue, Object... args) throws PrompterException;

    int i18nPromptForInt(String key, int defaultInt, Object... args) throws PrompterException;

    <T> T promptForChoice(String message, Iterable<T> choices, Function<T, String> choiceDisplay) throws PrompterException;
}
