package com.atlassian.ap3.api.template;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.config.Ap3ConfigurationManager;
import com.atlassian.ap3.api.config.KitConfigurationEntry;
import com.atlassian.ap3.api.config.OAuthConfigurationEntry;
import com.atlassian.ap3.api.oauth.OAuthKeyPair;
import com.atlassian.ap3.api.util.NIOFileUtils;
import com.atlassian.ap3.api.util.VelocityTemplateUtil;
import com.atlassian.ap3.spi.kit.KitDescriptor;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

@Named
public final class ProjectGeneratorImpl implements ProjectGenerator
{
    public static final Set<String> DEFAULT_TEMPLATE_IGNORE = ImmutableSet.<String> builder().add(
            "*.iml"
            ,"*.ipr"
            ,"*.iws"
            ,"*.log"
            ,"*.log.1"
            ,"*.orig"
            ,".idea"
            ,".project"
            ,".classpath"
            ,".settings"
            ,".DS_Store").build();
    
    public static final Set<String> DEFAULT_GIT_IGNORE = ImmutableSet.<String> builder().add("target").addAll(DEFAULT_TEMPLATE_IGNORE).build();

    private final Ap3ConfigurationManager configurationManager;

    @Inject
    public ProjectGeneratorImpl(Ap3ConfigurationManager configurationManager)
    {
        this.configurationManager = configurationManager;
    }

    @Override
    public Path generateProject(TemplateInfo templateInfo, Map<String, Object> context, PluginIdentifier pluginId, KitDescriptor kit, OAuthKeyPair keyPair) throws IOException
    {

        final Path workingDir = Paths.get("");
        final Path rootProjectDir = workingDir.resolve(pluginId.getDashedName());

        Files.createDirectories(rootProjectDir);
        
        Path templateDir = templateInfo.getTemplateDir();

        processVelocityTemplates(templateDir, rootProjectDir, context);
        
        processStaticFiles(templateDir, rootProjectDir, context, getTemplateIgnores(templateInfo));

        writeGitIgnore(rootProjectDir,getGitIgnores(templateInfo));

        writeAp3ProjectFile(kit, keyPair, rootProjectDir);
        
        return rootProjectDir;
    }

    private void writeAp3ProjectFile(KitDescriptor kit, OAuthKeyPair keyPair, Path rootProjectDir) throws IOException
    {
        KitConfigurationEntry kitEntry = new KitConfigurationEntry();
        kitEntry.setId(kit.getKitId());
        
        configurationManager.saveProjectEntry(kitEntry,rootProjectDir);

        if(null != keyPair && StringUtils.isNotBlank(keyPair.getPrivateKey()))
        {
            OAuthConfigurationEntry oauthEntry = new OAuthConfigurationEntry();
            oauthEntry.setPvtkey(keyPair.getPrivateKey());
            oauthEntry.setPubkey(keyPair.getPublicKey());
            
            configurationManager.saveProjectEntry(oauthEntry,rootProjectDir);
        }
    }

    private void writeGitIgnore(Path rootProjectDir, Set<String> gitIgnores) throws IOException
    {
        StringBuilder sb = new StringBuilder();
        String newline = System.getProperty("line.separator");
        
        for(String ignore : gitIgnores)
        {
            sb.append(ignore).append(newline);
        }
        
        Path gitignore = rootProjectDir.resolve(".gitignore");
        Files.write(gitignore, sb.toString().getBytes(), StandardOpenOption.CREATE);
    }

    private Set<String> getTemplateIgnores(TemplateInfo templateInfo)
    {
        Set<String> templateIgnores = null;
        
        if(!templateInfo.getTemplateignore().isEmpty())
        {
            templateIgnores = ImmutableSet.<String> builder().add(".ap3").add(AP3TemplateManager.TEMPLATE_INFO_FILE).addAll(templateInfo.getTemplateignore()).build();
        }
        else
        {
            templateIgnores = ImmutableSet.<String> builder().add(".ap3").add(AP3TemplateManager.TEMPLATE_INFO_FILE).addAll(DEFAULT_TEMPLATE_IGNORE).build();
        }
        
        return templateIgnores;
    }

    private Set<String> getGitIgnores(TemplateInfo templateInfo)
    {
        Set<String> gitIgnores = DEFAULT_GIT_IGNORE;

        if(!templateInfo.getGitingore().isEmpty())
        {
            gitIgnores = ImmutableSet.<String> builder().addAll(templateInfo.getGitingore()).build();
        }
        return gitIgnores;
    }

    private void processStaticFiles(Path templateDir, Path rootProjectDir, Map<String, Object> context, Set<String> templateIgnores) throws IOException
    {
        Files.walkFileTree(templateDir, new StaticVisitor(rootProjectDir, templateDir, context, templateIgnores));
    }

    private void processVelocityTemplates(Path templateDir, Path rootProjectDir, Map<String, Object> context) throws IOException
    {
        Files.walkFileTree(templateDir, new VelocityVisitor(rootProjectDir, templateDir, context));
    }

    private String expandFolders(String path, Map<String, Object> context)
    {
        String[] pathParts = StringUtils.split(path, File.separator);

        StringBuilder sb = new StringBuilder();

        for (String part : pathParts)
        {
            String varName = getFolderVariableName(part);
            if (null != varName && context.containsKey(varName))
            {
                String replacePath = (String) context.get(varName);
                sb.append(FilenameUtils.separatorsToSystem(replacePath)).append(File.separator);
            }
            else
            {
                sb.append(part).append(File.separator);
            }
        }

        return sb.toString();
    }

    private String getFolderVariableName(String folderName)
    {
        if (folderName.startsWith("_") && folderName.endsWith("_") && folderName.length() > 2)
        {
            String varName = StringUtils.substringBetween(folderName, "_");
            if (StringUtils.isNotBlank(varName))
            {
                return varName;
            }
        }

        return null;
    }

    private class VelocityVisitor extends SimpleFileVisitor<Path>
    {
        private final Path rootProjectDir;
        private final Path templateDir;
        private final Map<String, Object> context;

        private VelocityVisitor(Path rootProjectDir, Path templateDir, Map<String, Object> context)
        {
            this.rootProjectDir = rootProjectDir;
            this.templateDir = templateDir;
            this.context = context;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException
        {
            String dirname = dir.getFileName().toString();
            
            if(StringUtils.startsWith(dirname, "."))
            {
                return FileVisitResult.SKIP_SUBTREE;
            }

            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
        {
            if (attrs.isRegularFile() && StringUtils.endsWith(file.getFileName().toString(),".parse"))
            {
                String templatePath = StringUtils.removeStart(file.toAbsolutePath().toString(), templateDir.toAbsolutePath().toString());
                String outputPath = FilenameUtils.removeExtension(templatePath);
                
                Path outputFile = rootProjectDir.resolve(expandFolders(outputPath, context));
                Files.createDirectories(outputFile.getParent());

                try
                {
                    String tmpl = VelocityTemplateUtil.parseTemplate(templatePath,context,templateDir.toAbsolutePath().toString());
                    NIOFileUtils.writeStringToFile(outputFile,tmpl);
                }
                catch (Exception e)
                {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }

            return FileVisitResult.CONTINUE;
        }
    }

    private class StaticVisitor extends SimpleFileVisitor<Path>
    {
        private final Path rootProjectDir;
        private final Path templateDir;
        private final Map<String, Object> context;
        private final Set<String> exactIgnores;
        private final Set<String> extensionIgnores;

        private StaticVisitor(Path rootProjectDir, Path templateDir, Map<String, Object> context, Set<String> templateIgnores)
        {
            this.rootProjectDir = rootProjectDir;
            this.templateDir = templateDir;
            this.context = context;
            
            this.exactIgnores = Sets.filter(templateIgnores,new Predicate<String>() {
                
                @Override
                public boolean apply(String input)
                {
                    return !input.startsWith("*");
                }
            });


            this.extensionIgnores = new HashSet<>();
            for(String ignore : templateIgnores)
            {
                if(ignore.startsWith("*."))
                {
                    extensionIgnores.add(StringUtils.substringAfter(ignore,"*."));
                }
            }
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException
        {
            String dirname = dir.getFileName().toString();

            if(StringUtils.startsWith(dirname, ".") || exactIgnores.contains(dirname))
            {
                return FileVisitResult.SKIP_SUBTREE;
            }

            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
        {
            String filename = file.getFileName().toString();
            String extension = FilenameUtils.getExtension(filename);
            
            if (attrs.isRegularFile() && !"parse".equals(extension) && !exactIgnores.contains(filename) && !extensionIgnores.contains(extension))
            {
                String templatePath = StringUtils.removeStart(file.toAbsolutePath().toString(), templateDir.toString());

                Path outputFile = rootProjectDir.resolve(expandFolders(templatePath, context));
                Files.createDirectories(outputFile.getParent());
                Files.copy(file,outputFile,StandardCopyOption.REPLACE_EXISTING);                
            }

            return FileVisitResult.CONTINUE;
        }
    }
}
