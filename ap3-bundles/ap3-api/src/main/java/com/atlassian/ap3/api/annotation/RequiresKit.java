package com.atlassian.ap3.api.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.atlassian.ap3.spi.kit.KitDescriptor;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(TYPE)
@Retention(RUNTIME)
@Documented
public @interface RequiresKit
{
    Class<? extends KitDescriptor> value();
}
