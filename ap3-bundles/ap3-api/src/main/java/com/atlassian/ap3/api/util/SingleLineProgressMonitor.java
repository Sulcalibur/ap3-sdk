package com.atlassian.ap3.api.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.lib.TextProgressMonitor;

/**
 * @since version
 */
public class SingleLineProgressMonitor extends TextProgressMonitor
{
    private final Writer out;

    private boolean write;
    
    private String lastLine;
    
    private String prefix = "";

    public SingleLineProgressMonitor()
    {
        this(new PrintWriter(System.out),"");
    }

    public SingleLineProgressMonitor(String prefix)
    {
        this(new PrintWriter(System.out), prefix);
    }

    public SingleLineProgressMonitor(Writer out, String prefix)
    {
        this.out = out;
        this.prefix = prefix;
        this.write = true;
    }

    @Override
    protected void onUpdate(String taskName, int workCurr) {
        StringBuilder s = new StringBuilder();
        format(s, taskName, workCurr);
        send(s);
    }

    @Override
    protected void onEndTask(String taskName, int workCurr) {
        StringBuilder s = new StringBuilder();
        format(s, taskName, workCurr);
        send(s);
    }

    @Override
    protected void onUpdate(String taskName, int cmp, int totalWork, int pcnt) {
        StringBuilder s = new StringBuilder();
        format(s, taskName, cmp, totalWork, pcnt);
        send(s);
    }

    @Override
    protected void onEndTask(String taskName, int cmp, int totalWork, int pcnt) {
        StringBuilder s = new StringBuilder();
        format(s, taskName, cmp, totalWork, pcnt);
        send(s);
    }
    
    /*

    @Override
    public void start(int totalTasks)
    {
        //ignore
    }

    @Override
    public void beginTask(String title, int totalWork)
    {
        this.totalWork = totalWork;
        this.taskName = title;
        
        StringBuilder s = new StringBuilder();
        format(s, title, 0);
        send(s);
    }

    @Override
    public void update(int completed)
    {
        int pcnt = (completed > 0 && completed < totalWork) ? completed * 100 / totalWork : 0;
        StringBuilder s = new StringBuilder();
        format(s, taskName, completed, totalWork, pcnt);
        send(s);
    }

    @Override
    public void endTask()
    {
        StringBuilder s = new StringBuilder();
        format(s, taskName, totalWork, totalWork, 100);
        send(s);
        clear();
        
        //send("\n");
        //clear();
    }

    @Override
    public boolean isCancelled()
    {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
    
    */
    
    private void format(StringBuilder s, String taskName, int workCurr)
    {
        s.append("\r");
        s.append(prefix);
        s.append(taskName);
        s.append(": ");
        while (s.length() < 25)
        { s.append(' '); }
        s.append(workCurr);
    }

    private void format(StringBuilder s, String taskName, int cmp,
                        int totalWork, int pcnt)
    {
        s.append("\r");
        s.append(prefix);
        s.append(taskName);
        s.append(": ");
        while (s.length() < 25)
        { s.append(' '); }

        String endStr = String.valueOf(totalWork);
        String curStr = String.valueOf(cmp);
        while (curStr.length() < endStr.length())
        { curStr = " " + curStr; }
        if (pcnt < 100)
        { s.append(' '); }
        if (pcnt < 10)
        { s.append(' '); }
        s.append(pcnt);
        s.append("% (");
        s.append(curStr);
        s.append("/");
        s.append(endStr);
        s.append(")");
    }

    private void send(StringBuilder s)
    {
        if (write)
        {
            try
            {
                clear();
                this.lastLine = s.toString();
                out.write(lastLine);
                out.flush();
            }
            catch (IOException err)
            {
                write = false;
            }
        }
    }

    public void clear()
    {
        if(StringUtils.isNotBlank(lastLine))
        {
            String empty = "\r" + StringUtils.repeat(' ', lastLine.length());
            if (write)
            {
                try
                {
                    out.write(empty);
                    out.flush();
                }
                catch (IOException err)
                {
                    write = false;
                }
            }
        }
    }

    
}
