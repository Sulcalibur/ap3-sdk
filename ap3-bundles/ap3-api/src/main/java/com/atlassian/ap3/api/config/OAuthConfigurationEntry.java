package com.atlassian.ap3.api.config;

import com.atlassian.ap3.api.annotation.ConfigurationEntry;

@ConfigurationEntry("oauth")
public class OAuthConfigurationEntry
{
    private String pvtkey;
    private String pubkey;

    public String getPvtkey()
    {
        return pvtkey;
    }

    public void setPvtkey(String pvtkey)
    {
        this.pvtkey = pvtkey;
    }

    public String getPubkey()
    {
        return pubkey;
    }

    public void setPubkey(String pubkey)
    {
        this.pubkey = pubkey;
    }
}
