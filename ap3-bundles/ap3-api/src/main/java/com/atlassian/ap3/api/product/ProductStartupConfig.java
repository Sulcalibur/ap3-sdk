package com.atlassian.ap3.api.product;

import java.nio.file.Path;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * @since version
 */
public class ProductStartupConfig
{
    private final String productVersion;
    private final int port;
    private final Path dataPath;
    private final String dataVersion;
    private final boolean cleanHome;
    private final boolean debug;
    private final boolean debugSuspend;
    private final int debugPort;
    private final List<String> jvmArgs;
    
    public ProductStartupConfig(String productVersion, int port, Path dataPath, String dataVersion, boolean cleanHome, boolean debug, boolean debugSuspend, int debugPort, List<String> jvmArgs)
    {
        this.productVersion = productVersion;
        this.port = port;
        this.dataPath = dataPath;
        this.dataVersion = dataVersion;
        this.cleanHome = cleanHome;
        this.debug = debug;
        this.debugSuspend = debugSuspend;
        this.debugPort = debugPort;
        this.jvmArgs = jvmArgs;
    }

    public String getProductVersion()
    {
        return productVersion;
    }

    public int getPort()
    {
        return port;
    }

    public Path getDataPath()
    {
        return dataPath;
    }

    public String getDataVersion()
    {
        return dataVersion;
    }

    public boolean doCleanHome()
    {
        return cleanHome;
    }

    public boolean doDebug()
    {
        return debug;
    }

    public boolean doDebugSuspend()
    {
        return debugSuspend;
    }

    public List<String> getJvmArgs()
    {
        return jvmArgs;
    }

    public int getDebugPort()
    {
        return debugPort;
    }

    public static Builder builder(String productVersion,int port)
    {
        return new Builder(productVersion,port);
    }
    
    public static  class Builder
    {
        private final String productVersion;
        private final int port;
        private Path dataPath;
        private String dataVersion;
        private boolean cleanHome;
        private boolean debug;
        private boolean debugSuspend;
        private int debugPort;
        private List<String> jvmArgs;

        public Builder(String productVersion, int port)
        {
            this.productVersion = productVersion;
            this.port = port;
            this.debugPort = 5005;
            this.debug = false;
            this.debugSuspend = true;
            this.jvmArgs = newArrayList();
            this.cleanHome = false;
        }

        public Builder setDataPath(Path dataPath)
        {
            this.dataPath = dataPath;
            return this;
        }

        public Builder setDataVersion(String dataVersion)
        {
            this.dataVersion = dataVersion;
            return this;
        }

        public Builder setCleanHome(boolean cleanHome)
        {
            this.cleanHome = cleanHome;
            return this;
        }

        public Builder setDebug(boolean debug)
        {
            this.debug = debug;
            return this;
        }

        public Builder setDebugSuspend(boolean debugSuspend)
        {
            this.debugSuspend = debugSuspend;
            return this;
        }

        public Builder setDebugPort(int port)
        {
            if(port > 0)
            {
                this.debugPort = port;
            }
            return this;
        }

        public Builder setJvmArgs(List<String> jvmArgs)
        {
            this.jvmArgs = jvmArgs;
            return this;
        }

        public ProductStartupConfig build()
        {
            return new ProductStartupConfig(productVersion,port,dataPath,dataVersion,cleanHome,debug,debugSuspend,debugPort,jvmArgs);
        }
    }
}
