package com.atlassian.ap3.api;

import java.util.Locale;

import com.google.inject.ImplementedBy;

@ImplementedBy(I18nResolverImpl.class)
public interface I18nResolver
{
    String getText(String key, Object... arguments);
    String getText(Locale locale, String key, Object... arguments);
}
