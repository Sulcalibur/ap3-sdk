package com.atlassian.ap3.api.util;

import java.io.IOException;
import java.util.Map;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.SubmoduleInitCommand;
import org.eclipse.jgit.api.SubmoduleUpdateCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ProgressMonitor;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.submodule.SubmoduleStatus;
import org.eclipse.jgit.submodule.SubmoduleWalk;

/**
 * @since version
 */
public class JGitUtil
{
    public static void addSubmoduleRecursively(Repository baseRepo, String submodule,String url, ProgressMonitor monitor) throws GitAPIException, IOException
    {
        
        Git git = new Git(baseRepo);
        Map<String,SubmoduleStatus> statuses = git.submoduleStatus().addPath(submodule).call();
        Repository subRepo;
        
        if(statuses.isEmpty())
        {
            subRepo = git.submoduleAdd().setPath(submodule).setURI(url).setProgressMonitor(monitor).call();
            cloneSubmodule(subRepo,monitor);
        }
        else
        {
            subRepo = SubmoduleWalk.getSubmoduleRepository(git.getRepository(),submodule);
            updateSubmodule(subRepo,monitor);
        }
    }

    private static void updateSubmodule(Repository repo, ProgressMonitor monitor) throws GitAPIException, IOException
    {

        Git git = new Git(repo);
        git.checkout().setName("master").call();
        git.pull().setProgressMonitor(monitor).call();
        
        SubmoduleWalk walk = SubmoduleWalk.forIndex(repo);
        while (walk.next()) {
            Repository subRepo = walk.getRepository();
            if (subRepo != null) {
                try {
                    cloneSubmodule(subRepo,monitor);
                } finally {
                    subRepo.close();
                }
            }
        }

        
       /* Git git = new Git(repo);

        if (!git.submoduleUpdate().setProgressMonitor(monitor).call().isEmpty()) {
            SubmoduleWalk walk = SubmoduleWalk.forIndex(git.getRepository());
            while (walk.next()) {
                Repository subRepo = walk.getRepository();
                if (subRepo != null) {
                    try {
                        updateSubmoduleRecursively(subRepo, monitor);
                    } finally {
                        subRepo.close();
                    }
                }
            }
        }*/
    }
    
    private static void cloneSubmodule(Repository repo, ProgressMonitor monitor) throws IOException, GitAPIException
    {
        Git git = new Git(repo);
        git.checkout().setName("master").call();
        git.pull().setProgressMonitor(monitor).call();
        
        SubmoduleInitCommand init = new SubmoduleInitCommand(repo);
        if (init.call().isEmpty())
        {
            return;
        }

        SubmoduleUpdateCommand update = new SubmoduleUpdateCommand(repo);
        update.setProgressMonitor(monitor);
        if (!update.call().isEmpty()) {
            SubmoduleWalk walk = SubmoduleWalk.forIndex(repo);
            while (walk.next()) {
                Repository subRepo = walk.getRepository();
                if (subRepo != null) {
                    try {
                        cloneSubmodule(subRepo,monitor);
                    } finally {
                        subRepo.close();
                    }
                }
            }
        }
    }

}
