package com.atlassian.ap3.spi.kit;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

/**
 * @since version
 */
public interface ProjectPackager
{
    Path packageProject(Path projectRoot, Path containerJar, String containerVersion, List<String> jvmArgs) throws IOException;
    Path getResourcesPath(Path projectRoot);
}
