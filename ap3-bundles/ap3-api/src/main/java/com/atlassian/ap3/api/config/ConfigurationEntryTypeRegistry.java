package com.atlassian.ap3.api.config;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.annotation.ConfigurationEntry;

import com.google.common.collect.ImmutableSet;

import org.eclipse.sisu.BeanEntry;

@Named
public class ConfigurationEntryTypeRegistry
{
    private final Set<Class<?>> entryTypes;
    private final Iterable<BeanEntry<ConfigurationEntry, Object>> entries;

    @Inject
    public ConfigurationEntryTypeRegistry(Iterable<BeanEntry<ConfigurationEntry, Object>> entries)
    {
        this.entries = entries;
        this.entryTypes = new HashSet<>();
    }

    public void registerEntryType(Class<?> entryClass)
    {
        entryTypes.add(entryClass);
    }

    public Set<Class<?>> getEntryTypes()
    {
        if (entryTypes.size() < 1)
        {
            for (BeanEntry<ConfigurationEntry, Object> entry : entries)
            {
                Class<?> clazz = entry.getImplementationClass();

                entryTypes.add(clazz);
            }
        }

        return ImmutableSet.copyOf(entryTypes);
    }

    private String getClassName(URL url)
    {
        String className = url.getPath();
        // remove leading "/"
        className = className.charAt(0) == '/' ? className.substring(1) : className;
        // all '/' to '.'
        className = className.replaceAll("/", ".");
        // remove '.class'
        return className.substring(0, className.length() - ".class".length());
    }
}
