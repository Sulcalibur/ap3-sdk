package com.atlassian.ap3.cli;

import javax.inject.Named;

import com.atlassian.ap3.api.command.Ap3CommandManager;
import com.atlassian.ap3.spi.command.Ap3Command;

import com.google.inject.Inject;

import io.airlift.command.CommandFactory;

@Named
public class Ap3CommandFactory implements CommandFactory<Ap3Command>
{
    private final Ap3CommandManager commandManager;

    @Inject
    public Ap3CommandFactory(Ap3CommandManager commandManager)
    {
        this.commandManager = commandManager;
    }
    
    @Override
    public Ap3Command createInstance(Class<?> type)
    {
        if(Ap3Help.class.equals(type))
        {
            return new Ap3Help();
        }
        
        return commandManager.getCommandByType(type);
    }
}
