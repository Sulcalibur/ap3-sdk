package com.atlassian.ap3.api.product;

import java.io.IOException;
import java.nio.file.Path;

/**
 * @since version
 */
public interface ProductStarter
{
    void startProduct(ProductStartupConfig config) throws IOException;
}
