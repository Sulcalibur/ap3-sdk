package com.atlassian.ap3.api.config;

import com.atlassian.ap3.api.annotation.ConfigurationEntry;

import org.eclipse.sisu.scanners.AbstractClassFinder;

/**
 * @since version
 */
public class ConfigurationEntryClassFinder extends AbstractClassFinder
{

    protected ConfigurationEntryClassFinder(boolean global)
    {
        super(global, ConfigurationEntry.class.getName());
    }
}
