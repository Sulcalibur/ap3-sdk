package com.atlassian.ap3.spi.command;

import java.util.List;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableList;

import io.airlift.command.Arguments;
import io.airlift.command.Group;
import io.airlift.command.Option;

import static com.google.common.collect.Lists.newArrayList;

@Group(name = "start", description = "Starts an application container", defaultCommand = StartDefaultCommand.class)
public abstract class AbstractStartCommand extends BaseAp3Command
{
    @Option(name = {"-p", "--port"}, title = "Port", description = "The port to start the app container on")
    protected Integer port = 0;

    @Option(name = {"-d", "--debug"}, title = "Debug", description = "Turns on debugging")
    protected boolean debug = false;

    @Option(name = {"-ds", "--debug-suspend"}, title = "Debug suspend", description = "If the debugger should wait for a connection or not")
    protected boolean debugSuspend = false;

    @Option(name = {"-dp", "--debug-port"}, title = "Debug port", description = "The port the debugger listens on")
    protected Integer debugPort = 0;

    @Arguments(title = "extra arguments",description = "Extra arguments to pass to the start process")
    List<String> extraArgs = newArrayList();
    
    protected List<String> getExtraJvmArgs()
    {
        return ImmutableList.copyOf(Collections2.filter(extraArgs,new Predicate<String>() {
            @Override
            public boolean apply(String input)
            {
                return input.startsWith("-");
            }
        }));
    }

    protected List<String> getExtraNonJvmArgs()
    {
        return ImmutableList.copyOf(Collections2.filter(extraArgs,new Predicate<String>() {
            @Override
            public boolean apply(String input)
            {
                return !input.startsWith("-");
            }
        }));
    }
    
}
