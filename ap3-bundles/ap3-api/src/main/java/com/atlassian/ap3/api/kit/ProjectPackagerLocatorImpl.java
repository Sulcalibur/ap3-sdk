package com.atlassian.ap3.api.kit;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.annotation.RequiresKit;
import com.atlassian.ap3.spi.kit.KitDescriptor;
import com.atlassian.ap3.spi.kit.ProjectPackager;

@Named
@Singleton
public class ProjectPackagerLocatorImpl implements ProjectPackagerLocator
{
    private final Map<String, ProjectPackager> packagers;
    private final KitDescriptorLocator kitLocator;
    
    @Inject
    public ProjectPackagerLocatorImpl(Map<String, ProjectPackager> packagers, KitDescriptorLocator kitLocator)
    {
        this.packagers = packagers;
        this.kitLocator = kitLocator;
    }

    @Override
    public ProjectPackager getPackager(String kitId)
    {
        for(Map.Entry<String,ProjectPackager> entry : packagers.entrySet())
        {
            ProjectPackager packager = entry.getValue();
            if(packager.getClass().isAnnotationPresent(RequiresKit.class))
            {
                KitDescriptor descriptor = kitLocator.getKitDescriptor(packager.getClass().getAnnotation(RequiresKit.class).value());
                if(null != descriptor && descriptor.getKitId().equals(kitId))
                {
                    return packager;
                }
            }
        }
        return null;
    }
}
