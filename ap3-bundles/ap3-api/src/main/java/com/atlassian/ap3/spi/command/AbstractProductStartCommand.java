package com.atlassian.ap3.spi.command;

import io.airlift.command.Option;

/**
 * @since version
 */
public abstract class AbstractProductStartCommand extends AbstractStartCommand
{
    @Option(name = {"-v", "--version"}, title = "Version", description = "The product version to use")
    protected String version = "";

    @Option(name = {"-ch", "--clean-home"}, title = "Clean Home", description = "TCleans the product home directory")
    protected boolean cleanHome = false;

    @Option(name = {"-pdv", "--product-data-version"}, title = "Product Data Version", description = "The product data version to use")
    protected String dataVersion = "";

    @Option(name = {"-pdp", "--product-data-path"}, title = "Product Data Path", description = "A path to a product home zip file to use")
    protected String dataPath = "";
}
