package com.atlassian.ap3.spi.command;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.Map;

import javax.inject.Inject;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.annotation.RequiresKit;
import com.atlassian.ap3.api.kit.KitDescriptorLocator;
import com.atlassian.ap3.api.oauth.OAuthKeyUtil;
import com.atlassian.ap3.api.oauth.OAuthKeyPair;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.api.template.*;
import com.atlassian.ap3.home.HomeLocator;
import com.atlassian.ap3.spi.kit.KitDescriptor;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import org.apache.commons.lang3.StringUtils;

import io.airlift.command.Group;
import io.airlift.command.Option;

@Group(name = "new", description = "Creates a new Atlassian Plugin", defaultCommand = NewDefaultCommand.class)
public abstract class AbstractProjectCreatorCommand extends BaseAp3Command
{
    @Option(name = {"-tn", "--template-name"}, title = "Template Name", description = "The name of the template to use")
    private String templateName;

    @Option(name = {"-tf", "--template-folder"}, title = "Template Folder", description = "The path of a local template to use")
    private String templatePath;

    @Option(name = {"-n", "--name"}, title = "Plugin Name", description = "The name of your new plugin")
    private String pluginName;

    @Option(name = {"-d", "--description"}, title = "Description", description = "The description for your new plugin")
    private String pluginDescription;

    @Inject
    protected HomeLocator homeLocator;

    @Inject
    protected AP3TemplateManager templateManager;

    @Inject
    protected Prompter prompter;

    @Inject
    protected ProjectGenerator projectGenerator;

    @Inject
    protected KitDescriptorLocator kitLocator;

    @Inject
    private OAuthKeyUtil keyGenerator;
    
    public TemplateInfo getTemplateInfo() throws Ap3Exception
    {
        if(StringUtils.isNotBlank(templatePath))
        {
            Path templateFolder = Paths.get(templatePath);
            if(Files.exists(templateFolder) && Files.isReadable(templateFolder))
            {
                return getTemplateFromPath(templateFolder);
            }
        }
        
        return getTemplateByName();
    }

    @Override
    public void run() throws Ap3Exception
    {
       
        TemplateInfo templateInfo = getTemplateInfo();
        if(null == templateInfo)
        {
            System.out.println("Template not found!");
            return;
        }

        PluginIdentifier pluginId = getPluginIdentifier();
        
        Map<String,Object> context = createContext(templateInfo,pluginId);

        mergeTemplateVariables(templateInfo,pluginId,context);
        
        Path projectRoot = createProject(templateInfo,pluginId,context);
        
        prompter.showInfo("Project successfully created in: " + projectRoot.toAbsolutePath().toString());
    }

    protected abstract Map<String,Object> createContext(TemplateInfo templateInfo, PluginIdentifier pluginId) throws Ap3Exception;
    
    protected Path createProject(TemplateInfo templateInfo, PluginIdentifier pluginId, Map<String,Object> context) throws Ap3Exception
    {
        try
        {
            if (!this.getClass().isAnnotationPresent(RequiresKit.class))
            {
                throw new Ap3Exception("RequiresKit annotation missing from command '" + this.getClass().getSimpleName() + "'");
            }
            
            Class<? extends KitDescriptor> kitClass = this.getClass().getAnnotation(RequiresKit.class).value();

            OAuthKeyPair keyPair = new OAuthKeyPair("","");
            
            if(templateInfo.getRequiresOauth())
            {
                keyPair = keyGenerator.generateKeys();
                context.put("oauthBlock",keyGenerator.getOAuthXmlBlock(keyPair));
            }
            
            return projectGenerator.generateProject(templateInfo,context,pluginId,kitLocator.getKitDescriptor(kitClass),keyPair);
        }
        catch (IOException | NoSuchAlgorithmException e)
        {
            throw new Ap3Exception("Error generating project", e);
        }
    }
    
    private TemplateInfo getTemplateFromPath(Path templateFolder) throws Ap3Exception
    {
        return templateManager.loadTemplateFromPath(templateFolder);
    }

    private TemplateInfo getTemplateByName() throws Ap3Exception
    {
        if (!this.getClass().isAnnotationPresent(RequiresKit.class))
        {
            throw new Ap3Exception("KitProvider annotation missing from command '" + this.getClass().getSimpleName() + "'");
        }

        Class<? extends KitDescriptor> kitClass = this.getClass().getAnnotation(RequiresKit.class).value();
        KitDescriptor kit = kitLocator.getKitDescriptor(kitClass);
        
        String repoUrl = kit.getTemplatesRepoUrl();
        
        Iterable<TemplateInfo> templates = templateManager.listKitTemplates(repoUrl, globalOptions.isOffline());
        
        if(null == templates || !templates.iterator().hasNext())
        {
            return null;
        }
        
        if (StringUtils.isBlank(templateName))
        {
            if (!templates.iterator().hasNext())
            {
                //TODO: not sure what to do here yet
                return null;
            }

            return promptForTemplate(templates);
        }
        else
        {
            Iterator<TemplateInfo> it = Iterables.filter(templates, new Predicate<TemplateInfo>()
            {
                @Override
                public boolean apply(TemplateInfo input)
                {
                    return templateName.equals(input.getKey());
                }
            }).iterator();

            if(it.hasNext())
            {
                return it.next();
            }
            else
            {
                System.out.println("Invalid template name");
                return promptForTemplate(templates);
            }
        }
    }

    public PluginIdentifier getPluginIdentifier() throws Ap3Exception
    {
        if(StringUtils.isBlank(pluginName))
        {
            try
            {
                this.pluginName = prompter.promptNotBlank("Enter your new plugin name");
            }
            catch (PrompterException e)
            {
                throw new Ap3Exception("Error prompting for plugin name", e);
            }
        }
        
        return new PluginIdentifier(pluginName);
    }
    
    protected void mergeTemplateVariables(TemplateInfo templateInfo, PluginIdentifier pluginId, Map<String, Object> context) throws Ap3Exception
    {
        if(StringUtils.isBlank(pluginDescription))
        {
            pluginDescription = "The " + pluginId.getName() + " plugin";    
        }

        context.put("pluginName",pluginId.getName());
        context.put("pluginDescription",pluginDescription);
        context.put("pluginFolder",pluginId.getDashedName());
        
        if(templateInfo.getVariables().isEmpty())
        {
            return;
        }
        
        for(TemplateVariable variable : templateInfo.getVariables())
        {
            if(context.containsKey(variable.getName()))
            {
                continue;
            }
            
            String value = null;
            try
            {
                if(StringUtils.isNotBlank(variable.getStaticValue()))
                {
                    value = variable.getStaticValue();
                }
                else if(!variable.getChoices().isEmpty())
                {
                    value = prompter.promptForChoice(variable.getPrompt(),variable.getChoices());
                }
                else
                {
                    value = prompter.promptNotBlank(variable.getPrompt(),variable.getDefaultValue());
                }

                context.put(variable.getName(),value);
            }
            catch (PrompterException e)
            {
                throw new Ap3Exception("Error prompting for template variable", e);
            }
        }
    }
    
    private TemplateInfo promptForTemplate(Iterable<TemplateInfo> templates) throws Ap3Exception
    {
        TemplateInfo template = null;
        try
        {
            template = prompter.promptForChoice("Choose a template", templates);
        }
        catch (PrompterException e)
        {
            throw new Ap3Exception("Error prompting for template", e);
        }

        return template;
    }
}
