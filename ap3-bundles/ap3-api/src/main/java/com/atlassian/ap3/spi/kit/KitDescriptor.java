package com.atlassian.ap3.spi.kit;

/**
 * @since version
 */
public interface KitDescriptor
{
    String getKitId();
    String getTemplatesRepoUrl();
    
}
