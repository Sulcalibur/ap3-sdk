package com.atlassian.ap3.spi.command;

import javax.inject.Inject;

import com.atlassian.ap3.api.Ap3Logger;
import com.atlassian.ap3.api.GlobalOptions;

import org.eclipse.sisu.Nullable;
import org.slf4j.Logger;

/**
 * @since version
 */
public abstract class BaseAp3Command implements Ap3Command
{
    @Inject
    @Nullable
    protected GlobalOptions globalOptions = new GlobalOptions();
    
    @Inject
    private Ap3Logger logger;
    
    protected Logger getLog()
    {
        return logger;
    }
}
