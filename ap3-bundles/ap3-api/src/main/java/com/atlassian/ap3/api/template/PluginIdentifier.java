package com.atlassian.ap3.api.template;

import com.atlassian.ap3.api.util.NamingUtil;

/**
 * @since version
 */
public class PluginIdentifier
{
    private final String name;
    private final String dashedName;
    private final String camelName;
    private final String cappedCamelName;

    public PluginIdentifier(String name)
    {
        this.name = name;
        this.dashedName = NamingUtil.camelCaseOrSpaceToDashed(name);
        this.camelName = NamingUtil.camelCase(name);
        this.cappedCamelName = NamingUtil.cappedCamelCase(name);
    }

    public String getName()
    {
        return name;
    }

    public String getDashedName()
    {
        return dashedName;
    }

    public String getCamelName()
    {
        return camelName;
    }

    public String getCappedCamelName()
    {
        return cappedCamelName;
    }
}
