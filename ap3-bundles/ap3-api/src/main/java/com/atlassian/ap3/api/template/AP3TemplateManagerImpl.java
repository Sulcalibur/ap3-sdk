package com.atlassian.ap3.api.template;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.util.JGitUtil;
import com.atlassian.ap3.api.util.NIOFileUtils;
import com.atlassian.ap3.api.util.SingleLineProgressMonitor;
import com.atlassian.ap3.home.HomeLocator;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

@Named
public class AP3TemplateManagerImpl implements AP3TemplateManager
{
    private final HomeLocator homeLocator;

    @Inject
    public AP3TemplateManagerImpl(HomeLocator homeLocator)
    {
        this.homeLocator = homeLocator;
    }

    @Override
    public Iterable<TemplateInfo> listKitTemplates(String kitUrl, boolean isOffline) throws Ap3Exception
    {
        List<TemplateInfo> info = new ArrayList<>();

        Path templatesDir = getKitTemplateFolder(kitUrl, isOffline);
        Gson gson = new GsonBuilder().create();

        try
        {
            Files.walkFileTree(templatesDir, EnumSet.noneOf(FileVisitOption.class), 2, new TemplateVisitor(gson, info));
        }
        catch (IOException e)
        {
            //ignore it
        }

        return ImmutableList.copyOf(info);
    }

    @Override
    public Path getKitTemplateFolder(String kitUrl, boolean isOffline) throws Ap3Exception
    {
        String baseTemplatesDirName = StringUtils.substringBeforeLast(StringUtils.substringAfterLast(kitUrl, "/"), ".");
        Path baseTemplatesDir = null;
        Path configTemplatesDir = null;

        try
        {
            configTemplatesDir = homeLocator.getConfigTemplateDirectory();
            if(NIOFileUtils.directoryIsEmpty(configTemplatesDir))
            {
                cloneBaseTemplates(configTemplatesDir);    
            }
            
            baseTemplatesDir = configTemplatesDir.resolve(baseTemplatesDirName);
            Files.createDirectories(baseTemplatesDir);
        }
        catch (Exception e)
        {
            throw new Ap3Exception("unable to locate templates directory", e);
        }


        if (!isOffline)
        {
            Repository baseRepo = null;
            try
            {
                String prefix = "retrieving templates... ";
                PrintWriter pw = new PrintWriter(System.out);
                pw.write(prefix);
                pw.flush();

                FileRepositoryBuilder builder = new FileRepositoryBuilder();
                builder.setGitDir(configTemplatesDir.resolve(Constants.DOT_GIT).toFile()).readEnvironment();

                baseRepo = builder.build();

                SingleLineProgressMonitor pm = new SingleLineProgressMonitor(pw, prefix);

                JGitUtil.addSubmoduleRecursively(baseRepo, baseTemplatesDirName, kitUrl, pm);


                pm.clear();
                pw.write("\r");
                pw.write(StringUtils.repeat(' ',prefix.length()));
                pw.write("\r");
                pw.flush();

            }
            catch (GitAPIException | IOException e)
            {
                throw new Ap3Exception("Unable to retrieve " + baseTemplatesDirName + " templates", e);
            }
            finally
            {
                if (null != baseRepo)
                {
                    baseRepo.close();
                }
            }
        }


        return baseTemplatesDir;
    }

    private void cloneBaseTemplates(Path configTemplatesDir) throws Ap3Exception
    {
        try
        {
            String prefix = "cloning base templates... ";
            PrintWriter pw = new PrintWriter(System.out);
            pw.write(prefix);
            pw.flush();

            SingleLineProgressMonitor pm = new SingleLineProgressMonitor(pw, prefix);

            CloneCommand cloneCommand = new CloneCommand();
            cloneCommand.setDirectory(configTemplatesDir.toFile());
            cloneCommand.setURI("https://bitbucket.org/atlassian/ap3-sdk-templates.git");
            cloneCommand.setProgressMonitor(pm);
            cloneCommand.setCloneSubmodules(true);
            
            cloneCommand.call();


            pm.clear();
            pw.write("\r");
            pw.write(StringUtils.repeat(' ',prefix.length()));
            pw.write("\r");
            pw.flush();

        }
        catch (GitAPIException e)
        {
            throw new Ap3Exception("Unable to retrieve base templates", e);
        }
        
    }

    @Override
    public TemplateInfo loadTemplateFromPath(Path templateFolder) throws Ap3Exception
    {
        Gson gson = new GsonBuilder().create();
        Path infoFile = templateFolder.resolve(TEMPLATE_INFO_FILE);
        if (Files.exists(infoFile) && Files.isReadable(infoFile))
        {
            try
            {
                String infoJson = NIOFileUtils.readFileToString(infoFile);
                TemplateInfo templateInfo = gson.fromJson(infoJson, TemplateInfo.class);
                templateInfo.setTemplateDir(templateFolder);

                return templateInfo;
            }
            catch (IOException e)
            {
                throw new Ap3Exception("Error reading template info", e);
            }
        }
        else
        {
            throw new Ap3Exception("Invalid template folder '" + templateFolder + "'");
        }
    }

    private class TemplateVisitor extends SimpleFileVisitor<Path>
    {
        private final Gson gson;
        private final List<TemplateInfo> info;

        private TemplateVisitor(Gson gson, List<TemplateInfo> info)
        {
            this.gson = gson;
            this.info = info;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException
        {
            String dirname = dir.getFileName().toString();

            if (StringUtils.startsWith(dirname, "."))
            {
                return FileVisitResult.SKIP_SUBTREE;
            }

            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
        {
            if (attrs.isRegularFile() && TEMPLATE_INFO_FILE.equals(file.getFileName().toString()))
            {
                try
                {
                    String infoJson = FileUtils.readFileToString(file.toFile());
                    TemplateInfo templateInfo = gson.fromJson(infoJson, TemplateInfo.class);
                    templateInfo.setTemplateDir(file.getParent());

                    info.add(templateInfo);
                }
                catch (IOException e)
                {
                    //just don't include it
                }
            }

            return FileVisitResult.CONTINUE;
        }
    }

}
