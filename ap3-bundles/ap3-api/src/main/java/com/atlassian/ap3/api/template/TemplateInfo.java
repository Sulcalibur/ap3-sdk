package com.atlassian.ap3.api.template;

import java.nio.file.Path;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

/**
 * @since version
 */
public class TemplateInfo
{
    private String key;
    private String name;
    private String description;
    private Set<String> gitingore;
    private Set<String> templateignore;
    private List<TemplateVariable> variables;
    private Boolean requiresOauth;
    
    private transient Path templateDir;

    public String getKey()
    {
        return key;
    }

    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public Path getTemplateDir()
    {
        return templateDir;
    }

    public Set<String> getGitingore()
    {
        if(null == gitingore)
        {
            gitingore = ImmutableSet.<String> of();
        }
        return gitingore;
    }

    public Set<String> getTemplateignore()
    {
        if(null == templateignore)
        {
            templateignore = ImmutableSet.<String> of();
        }
        
        return templateignore;
    }

    public List<TemplateVariable> getVariables()
    {
        if(null == variables)
        {
            variables = ImmutableList.<TemplateVariable> of();
        }
        
        return variables;
    }

    public Boolean getRequiresOauth()
    {
        if(null == requiresOauth)
        {
            requiresOauth = false;    
        }
        
        return requiresOauth;
    }

    void setTemplateDir(Path dir)
    {
        this.templateDir = dir;
    }

    @Override
    public String toString()
    {
        return "[" + key + "] " + name + " - " + description;
    }
}
