package com.atlassian.ap3.api.template;


import java.nio.file.Path;

import com.atlassian.ap3.api.Ap3Exception;

public interface AP3TemplateManager
{
    public static final String TEMPLATE_INFO_FILE = "template.info";
    
    Iterable<TemplateInfo> listKitTemplates(String kitUrl, boolean isOffline) throws Ap3Exception;
    Path getKitTemplateFolder(String kitUrl, boolean isOffline) throws Ap3Exception;

    TemplateInfo loadTemplateFromPath(Path templateFolder) throws Ap3Exception;
}
