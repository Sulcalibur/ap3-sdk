package ut.com.atlassian.ap3.api;

import java.util.List;

import com.atlassian.ap3.api.util.JVMArgsUtil;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @since version
 */
public class JVMArgsTest
{

    @Test
    public void testArgs() throws Exception
    {
        List<String> defaultArgs = ImmutableList.<String> builder()
                .add("-Xdebug")
                .add("-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005")
                .add("-Xmx512m")
                .add("-XX:MaxPermSize=256m")
                .add("-XX:+HeapDumpOnOutOfMemoryError")
                .add("-Datlassian.dev.mode=true")
                .build();

        List<String> userArgs = ImmutableList.<String> builder()
                .add("-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5006")
                .add("-Datlassian.dev.mode=false").add("-Xms512m")
                .build();

        List<String> mergedArgs = JVMArgsUtil.mergeToList(defaultArgs,userArgs);

       
        assertTrue(mergedArgs.contains("-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5006"));
        assertTrue(mergedArgs.contains("-Datlassian.dev.mode=false"));
        assertTrue(mergedArgs.contains("-Xms512m"));
        assertTrue(mergedArgs.contains("-Xmx512m"));
        assertTrue(mergedArgs.contains("-Xdebug"));
        assertTrue(mergedArgs.contains("-XX:MaxPermSize=256m"));
        
        assertFalse(mergedArgs.contains("-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"));
        assertFalse(mergedArgs.contains("-Datlassian.dev.mode=true"));

    }
}
