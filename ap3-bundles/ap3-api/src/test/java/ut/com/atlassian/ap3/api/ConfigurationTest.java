package ut.com.atlassian.ap3.api;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.ap3.api.config.Ap3Configuration;
import com.atlassian.ap3.ext.gson.Ap3ConfigEntryObjectAdapterFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Test;

import ut.com.atlassian.ap3.api.config.SimpleConfigEntry;
import ut.com.atlassian.ap3.api.config.UnlabledConfigEntry;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @since version
 */
public class ConfigurationTest
{
    @Test
    public void configReturnsProperType() throws Exception
    {
        SimpleConfigEntry simpleEntry = new SimpleConfigEntry();
        simpleEntry.setName("wow");
        
        Map<String,Object> entryList = new HashMap<>();
        entryList.put("simple", simpleEntry);

        Ap3Configuration config = new Ap3Configuration(entryList);
        
        SimpleConfigEntry typedEntry = config.getConfigurationEntry(SimpleConfigEntry.class);
        
        assertNotNull(typedEntry);
    }

    @Test
    public void gsonHandlesTypes() throws Exception
    {
        SimpleConfigEntry simpleEntry = new SimpleConfigEntry();
        simpleEntry.setName("wow");

        Map<String,Object> entryList = new HashMap<>();
        entryList.put("simple", simpleEntry);

        Ap3Configuration config = new Ap3Configuration(entryList);

        Ap3ConfigEntryObjectAdapterFactory adapter = new Ap3ConfigEntryObjectAdapterFactory();
        adapter.registerConfigurationType(SimpleConfigEntry.class);
        
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(adapter);
       // builder.setPrettyPrinting();
        
        Gson gson = builder.create();
        String json = gson.toJson(config);
        
        assertNotNull(json);
        
        Ap3Configuration parsedConfig = gson.fromJson(json,Ap3Configuration.class);

        SimpleConfigEntry typedEntry = parsedConfig.getConfigurationEntry(SimpleConfigEntry.class);

        assertNotNull(typedEntry);
    }

    @Test
    public void missingTypeDoesNotThrow() throws Exception
    {
        String json = "{\"ap3Configuration\":{\"simple\":{\"name\":\"wow\"},\"missingEntry\":{\"cool\":\"junk\"}}}";

        Ap3ConfigEntryObjectAdapterFactory adapter = new Ap3ConfigEntryObjectAdapterFactory();
        adapter.registerConfigurationType(SimpleConfigEntry.class);

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(adapter);
        builder.setPrettyPrinting();

        Gson gson = builder.create();

        Ap3Configuration parsedConfig = gson.fromJson(json,Ap3Configuration.class);

        assertNotNull(parsedConfig);

        SimpleConfigEntry typedEntry = parsedConfig.getConfigurationEntry(SimpleConfigEntry.class);

        assertNotNull(typedEntry);
    }
    
    @Test
    public void missingLabelAnnotationUsesClassname()
    {
        UnlabledConfigEntry entry = new UnlabledConfigEntry();
        entry.setName("not me");

        Map<String,Object> entryList = new HashMap<>();
        entryList.put(UnlabledConfigEntry.class.getSimpleName(), entry);

        Ap3Configuration config = new Ap3Configuration(entryList);

        Ap3ConfigEntryObjectAdapterFactory adapter = new Ap3ConfigEntryObjectAdapterFactory();
        adapter.registerConfigurationType(UnlabledConfigEntry.class);

        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(adapter);

        Gson gson = builder.create();
        String json = gson.toJson(config);

        assertNotNull(json);
        assertTrue("simple class name not found", json.contains(UnlabledConfigEntry.class.getSimpleName()));

        Ap3Configuration parsedConfig = gson.fromJson(json,Ap3Configuration.class);

        UnlabledConfigEntry typedEntry = parsedConfig.getConfigurationEntry(UnlabledConfigEntry.class);

        assertNotNull(typedEntry);
    }
}
