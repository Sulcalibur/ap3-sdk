package com.atlassian.ap3.ringokit;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.ContainerResolver;
import com.atlassian.ap3.api.annotation.RequiresKit;
import com.atlassian.ap3.api.kit.BundlePackageBuilder;
import com.atlassian.ap3.api.util.ZipFileSystemUtil;
import com.atlassian.ap3.home.HomeLocator;
import com.atlassian.ap3.spi.kit.ProjectPackager;

import org.apache.commons.lang.StringUtils;

import static com.google.common.collect.Lists.newArrayList;

@Named
@Singleton
@RequiresKit(RingoKitDescriptor.class)
public class RingoKitProjectPackager implements ProjectPackager
{
    private final HomeLocator homeLocator;
    
    @Inject
    public RingoKitProjectPackager(ContainerResolver containerResolver, HomeLocator homeLocator)
    {
        this.homeLocator = homeLocator;
    }

    @Override
    public Path packageProject(Path projectRoot, Path containerJar, String containerVersion, List<String> jvmArgs) throws IOException
    {
        
        String bundledLibs = generateBundledLibsEntry(projectRoot);
        
        try(FileSystem zip = ZipFileSystemUtil.openZipFileSystem(containerJar))
        {

            Path ringojs = extractFileFromContainer(zip, "ringojs.jar", "ringojs-" + containerVersion + ".jar");
            Path kitCommon = extractFileFromContainer(zip, "remotable-plugins-kit-common.jar", "remotable-plugins-kit-common-" + containerVersion + ".jar");
            Path ringoKit = extractFileFromContainer(zip, "remotable-plugins-ringojs-kit.jar", "remotable-plugins-ringojs-kit-" + containerVersion + ".jar");
            
           BundlePackageBuilder bundleBuilder = new BundlePackageBuilder(projectRoot)
                    .addJar(ringojs)
                    .addJar(kitCommon)
                    .addJar(ringoKit);
            
            if(StringUtils.isNotBlank(bundledLibs))
            {
                bundleBuilder.addManifestEntry("Bundle-ClassPath",bundledLibs);
            }
    
            return bundleBuilder.build();
        }
    }

    @Override
    public Path getResourcesPath(Path projectRoot)
    {
        return projectRoot;
    }

    private String generateBundledLibsEntry(Path projectRoot) throws IOException
    {
        final Path libDir = projectRoot.resolve("lib");
        if (Files.exists(libDir))
        {
            final List<String> entries = newArrayList();
            entries.add(".");
            
            Files.walkFileTree(libDir,new SimpleFileVisitor<Path>(){
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException
                {
                    entries.add("lib/" + libDir.relativize(file).toString());
                    return FileVisitResult.CONTINUE;
                }
                
            });

            
            return StringUtils.join(entries, ',');
        }
        
        return "";
    }

    private Path extractFileFromContainer(FileSystem zip, String entry, String filename) throws IOException
    {
        Path containerDir = homeLocator.getContainerDirectory();
        Path jarFile = containerDir.resolve(filename);
        if(Files.notExists(jarFile))
        {
            Path entryFile = zip.getPath(entry);
            Files.copy(entryFile,jarFile, StandardCopyOption.REPLACE_EXISTING);
        }
        
        return jarFile;
    }

}
