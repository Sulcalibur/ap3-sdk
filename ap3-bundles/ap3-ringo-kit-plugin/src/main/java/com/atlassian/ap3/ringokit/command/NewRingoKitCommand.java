package com.atlassian.ap3.ringokit.command;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.annotation.RequiresKit;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.api.template.PluginIdentifier;
import com.atlassian.ap3.api.template.TemplateInfo;
import com.atlassian.ap3.api.util.NamingUtil;
import com.atlassian.ap3.ringokit.RingoKitDescriptor;
import com.atlassian.ap3.spi.command.AbstractProjectCreatorCommand;

import org.apache.commons.lang3.StringUtils;

import io.airlift.command.Command;
import io.airlift.command.Option;

@Named
@RequiresKit(RingoKitDescriptor.class)
@Command(name = "ringo-kit", description = "Creates a new ringo-kit plugin")
public class NewRingoKitCommand extends AbstractProjectCreatorCommand
{
    
    @Option(name = {"-ns", "--namespace"}, title = "Namespace", description = "The namespace for your new plugin")
    String namespace;

    @Override
    protected Map<String, Object> createContext(TemplateInfo templateInfo, PluginIdentifier pluginId) throws Ap3Exception
    {
        Map<String,Object> context = new HashMap<>();
        String ns = validateNamespace(promptForNamespaceIfNeeded());

        context.put("namespace",ns);

        return context;
    }

    
    private String validateNamespace(String ns) throws Ap3Exception
    {
        boolean isValid = !StringUtils.containsWhitespace(ns);
        
        if(isValid)
        {
            try
            {
                Paths.get(ns);
            }
            catch (InvalidPathException e)
            {
                isValid = false;
            }
        }
        
        if(!isValid)
        {
            ns = validateNamespace(prompter.promptNotBlank("Enter a valid namespace"));
        }

        return ns;
    }

    private String promptForNamespaceIfNeeded() throws Ap3Exception
    {
        if (StringUtils.isBlank(namespace))
        {
            try
            {
                namespace = prompter.promptNotBlank("Enter a namespace");
            }
            catch (PrompterException e)
            {
                throw new Ap3Exception("Error prompting for namespace", e);
            }
        }

        return namespace;
    }
}
