package com.atlassian.ap3.base.command;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.home.HomeLocator;
import com.atlassian.ap3.spi.command.BaseAp3Command;

import io.airlift.command.Command;

@Named
@Command(name = "bash-it", description = "Adds bash-it aliases for ap3")
public class BashItCommand extends BaseAp3Command
{
    private static final String ALIAS_FILENAME = "ap3.aliases.bash";
    private static final String COMPLETION_FILENAME = "ap3.completion.bash";
    private final HomeLocator homeLocator;
    private final Prompter prompter;

    @Inject
    public BashItCommand(HomeLocator homeLocator, Prompter prompter)
    {
        this.homeLocator = homeLocator;
        this.prompter = prompter;
    }

    @Override
    public void run() throws Ap3Exception
    {
        Path bashitRoot = Paths.get(System.getProperty("user.home")).resolve(".bash_it");
        if(Files.notExists(bashitRoot))
        {
            prompter.showWarning("Bash-It is not installed.");
            prompter.showMessage("Please visit https://github.com/revans/bash-it to learn about bash-it");
            return;
        }
        try
        {
            createAliases(bashitRoot);
            createCompletion(bashitRoot);
            
            prompter.showInfo("bash-it plugin installed! Restart your shell and type 'ap3-help' for alias help");
        }
        catch (IOException e)
        {
            throw new Ap3Exception("Error copying bash-it files",e);
        }
    }
    
    public void createAliases(Path bashitRoot) throws IOException
    {
        Path bashitAliases = bashitRoot.resolve("aliases");
        Path bashitAliasesAvailable = bashitAliases.resolve("available");
        Path bashitAliasesEnabled = bashitAliases.resolve("enabled");

        Files.createDirectories(bashitAliasesAvailable);
        Files.createDirectories(bashitAliasesEnabled);

        Path aliasFile = homeLocator.getHomeDirectory().resolve(ALIAS_FILENAME);
        if(Files.notExists(aliasFile) || !Files.isReadable(aliasFile))
        {
            throw new IOException("Cannot find " + ALIAS_FILENAME);
        }

        Path ap3Available = bashitAliasesAvailable.resolve(ALIAS_FILENAME);
        Path ap3Enabled = bashitAliasesEnabled.resolve(ALIAS_FILENAME);

        Files.deleteIfExists(ap3Available);
        Files.deleteIfExists(ap3Enabled);
        
        Files.copy(aliasFile,bashitAliasesAvailable.resolve(aliasFile.getFileName()), StandardCopyOption.REPLACE_EXISTING);

        Files.createSymbolicLink(ap3Enabled,ap3Available);
    }

    public void createCompletion(Path bashitRoot) throws IOException
    {
        Path bashitCompletion = bashitRoot.resolve("completion");
        Path bashitCompletionAvailable = bashitCompletion.resolve("available");
        Path bashitCompletionEnabled = bashitCompletion.resolve("enabled");

        Files.createDirectories(bashitCompletionAvailable);
        Files.createDirectories(bashitCompletionEnabled);

        Path completionFile = homeLocator.getHomeDirectory().resolve(COMPLETION_FILENAME);
        if(Files.notExists(completionFile) || !Files.isReadable(completionFile))
        {
            throw new IOException("Cannot find " + COMPLETION_FILENAME);
        }

        Path ap3Available = bashitCompletionAvailable.resolve(COMPLETION_FILENAME);
        Path ap3Enabled = bashitCompletionEnabled.resolve(COMPLETION_FILENAME);

        Files.deleteIfExists(ap3Available);
        Files.deleteIfExists(ap3Enabled);

        Files.copy(completionFile,bashitCompletionAvailable.resolve(completionFile.getFileName()), StandardCopyOption.REPLACE_EXISTING);

        Files.createSymbolicLink(ap3Enabled,ap3Available);
    }
}
