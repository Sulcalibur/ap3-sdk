package com.atlassian.ap3.base.product;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.product.ConfigFileReplacement;
import com.atlassian.ap3.api.product.ProductArtifact;
import com.atlassian.ap3.api.product.ProductContext;
import com.atlassian.ap3.api.product.ProductStartupConfig;
import com.atlassian.ap3.spi.product.AbstractProductStarter;

import com.google.common.collect.ImmutableMap;

@Named
@Singleton
public class ConfluenceProductStarter extends AbstractProductStarter
{
    public static final String CONFLUENCE_ID = "confluence";
    public static final String CONFLUENCE_GROUPID = "com.atlassian.confluence";
    public static final String CONFLUENCE_ARTIFACTID = "confluence-webapp";
    public static final String RESOURCES_GROUPID = CONFLUENCE_GROUPID + ".plugins";
    public static final String RESOURCES_ARTIFACTID = "confluence-plugin-test-resources";
    
    @Override
    protected String getProductId()
    {
        return CONFLUENCE_ID;
    }

    @Override
    protected String getContextPath()
    {
        return "/" + CONFLUENCE_ID;
    }

    @Override
    protected Map<String, String> getSystemProperties(ProductContext ctx)
    {
        return ImmutableMap.<String, String> builder()
                           .put("confluence.home",ctx.getHomeDir().toAbsolutePath().toString())
                           .build();
    }

    @Override
    public void startProduct(ProductStartupConfig config) throws IOException
    {
        ProductArtifact product = new ProductArtifact(CONFLUENCE_GROUPID,CONFLUENCE_ARTIFACTID);
        ProductArtifact resources = new ProductArtifact(RESOURCES_GROUPID,RESOURCES_ARTIFACTID);

        super.startProduct(product,resources,config);
    }

    @Override
    public List<Path> getConfigFiles(Path homeDir)
    {
        Path dbDir = homeDir.resolve("database");
        
        List<Path> configFiles = super.getConfigFiles(homeDir);
        configFiles.add(dbDir.resolve("confluencedb.script"));
        configFiles.add(dbDir.resolve("confluencedb.log"));
        configFiles.add(homeDir.resolve("confluence.cfg.xml"));

        return configFiles;
    }

    @Override
    public List<ConfigFileReplacement> getConfigReplacements(ProductContext ctx)
    {
        Path homeDir = ctx.getHomeDir();
        String contextPath = getContextPath();

        String baseUrl = "http://localhost:" + ctx.getPort() + contextPath;

        List<ConfigFileReplacement> replacements = super.getConfigReplacements(ctx);

        replacements.add(new ConfigFileReplacement("@project-dir@", homeDir.getParent().toAbsolutePath().toString()));
        replacements.add(new ConfigFileReplacement("/confluence-home/", "/confluence-home-" + ctx.getPort() + "/", false));
        replacements.add(new ConfigFileReplacement("<baseUrl>http://localhost:1990/confluence</baseUrl>", "<baseUrl>" + baseUrl + "</baseUrl>", false));
        replacements.add(new ConfigFileReplacement("<baseUrl>http://localhost:8080</baseUrl>", "<baseUrl>" + baseUrl + "</baseUrl>", false));
        
        return replacements;
    }
}
