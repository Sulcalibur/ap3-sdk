package com.atlassian.ap3.base.product;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import javax.inject.Named;
import javax.inject.Singleton;

import com.atlassian.ap3.api.product.ConfigFileReplacement;
import com.atlassian.ap3.api.product.ProductArtifact;
import com.atlassian.ap3.api.product.ProductContext;
import com.atlassian.ap3.api.product.ProductStartupConfig;
import com.atlassian.ap3.spi.product.AbstractProductStarter;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

import org.apache.commons.lang3.StringUtils;

@Named
@Singleton
public class JiraProductStarter extends AbstractProductStarter
{
    public static final String JIRA_ID = "jira";
    public static final String JIRA_GROUPID = "com.atlassian.jira";
    public static final String JIRA_ARTIFACTID = "atlassian-jira-webapp";
    public static final String RESOURCES_GROUPID = JIRA_GROUPID + ".plugins";
    public static final String RESOURCES_ARTIFACTID = "jira-plugin-test-resources";
    
    @Override
    protected String getProductId()
    {
        return JIRA_ID;
    }

    @Override
    protected String getContextPath()
    {
        return "/" + JIRA_ID;
    }

    @Override
    protected Map<String, String> getSystemProperties(ProductContext ctx)
    {
        return ImmutableMap.<String, String> builder()
                .put("jira.home",ctx.getHomeDir().toAbsolutePath().toString())
                .put("catalina.servlet.uriencoding", "UTF-8")
                .put("cargo.datasource.datasource",getDatasourceString(ctx)).build();
    }

    private String getDatasourceString(ProductContext ctx)
    {
        List<String> cargoProperties = Lists.newArrayList();
        cargoProperties.add("cargo.datasource.url=jdbc:hsqldb:" + ctx.getHomeDir().toAbsolutePath().toString() + "/database");
        cargoProperties.add("cargo.datasource.driver=org.hsqldb.jdbcDriver");
        cargoProperties.add("cargo.datasource.username=sa");
        cargoProperties.add("cargo.datasource.password=");
        cargoProperties.add("cargo.datasource.jndi=jdbc/JiraDS");
        cargoProperties.add("cargo.datasource.type=javax.sql.DataSource");


        return StringUtils.join(cargoProperties, "|");
    }

    @Override
    public void startProduct(ProductStartupConfig config) throws IOException
    {
        ProductArtifact product = new ProductArtifact(JIRA_GROUPID,JIRA_ARTIFACTID);
        ProductArtifact resources = new ProductArtifact(RESOURCES_GROUPID,RESOURCES_ARTIFACTID);
        
        super.startProduct(product,resources,config);
    }

    @Override
    public List<Path> getConfigFiles(Path homeDir)
    {
        List<Path> configFiles = super.getConfigFiles(homeDir);
        configFiles.add(homeDir.resolve("database.log"));
        configFiles.add(homeDir.resolve("database.script"));
        configFiles.add(homeDir.resolve("dbconfig.xml"));
        return configFiles;
    }

    @Override
    public List<ConfigFileReplacement> getConfigReplacements(ProductContext ctx)
    {
        String contextPath = getContextPath();

        String baseUrl = "http://localhost:" + ctx.getPort() + contextPath;

        List<ConfigFileReplacement> replacements = super.getConfigReplacements(ctx);

        // We don't rewrap snapshots with these values:
        replacements.add(0, new ConfigFileReplacement("http://localhost:8080", baseUrl, false));
        replacements.add(new ConfigFileReplacement("/jira-home/", "/jira-home-" + ctx.getPort() + "/", false));
        replacements.add(new ConfigFileReplacement("@base-url@",baseUrl, false));
        return replacements;
    }

}
