package com.atlassian.ap3.base.command;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import com.atlassian.ap3.api.Ap3Exception;
import com.atlassian.ap3.api.ContainerResolver;
import com.atlassian.ap3.api.config.Ap3ConfigurationManager;
import com.atlassian.ap3.api.config.KitConfigurationEntry;
import com.atlassian.ap3.api.config.OAuthConfigurationEntry;
import com.atlassian.ap3.api.kit.ProjectPackagerLocator;
import com.atlassian.ap3.api.prompt.Prompter;
import com.atlassian.ap3.api.prompt.PrompterException;
import com.atlassian.ap3.api.util.JVMArgsUtil;
import com.atlassian.ap3.api.util.NIOFileUtils;
import com.atlassian.ap3.home.HomeLocator;
import com.atlassian.ap3.spi.command.AbstractStartCommand;
import com.atlassian.ap3.spi.kit.ProjectPackager;

import com.google.common.collect.ImmutableList;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import io.airlift.command.Command;
import io.airlift.command.Option;

@Named
@Command(name = "p3", description = "Starts the Plugins 3 container")
public class P3ContainerStartCommand extends AbstractStartCommand
{
    @Option(name = {"-f", "--force"}, title = "Force Update", description = "forces a container update")
    private boolean forceUpdate = false;

    @Option(name = {"-V", "--version"}, title = "Version", description = "The container version to use")
    private String version = "LATEST";

    @Option(name = {"-v"}, title = "Verbosity (-v | -vv | -vvv)", description = "Set the logging verbosity")
    private boolean verbosity1;

    @Option(name = {"-vv"}, hidden = true)
    private boolean verbosity2;

    @Option(name = {"-vvv"}, hidden = true)
    private boolean verbosity3;

    private final Prompter prompter;

    private final Ap3ConfigurationManager configurationManager;

    private final ProjectPackagerLocator packagerLocator;

    private final ContainerResolver containerResolver;
    
    private final HomeLocator homeLocator;

    @Inject
    public P3ContainerStartCommand(Prompter prompter, Ap3ConfigurationManager configurationManager, ProjectPackagerLocator packagerLocator, ContainerResolver containerResolver, HomeLocator homeLocator)
    {
        this.prompter = prompter;
        this.configurationManager = configurationManager;
        this.packagerLocator = packagerLocator;
        this.containerResolver = containerResolver;
        this.homeLocator = homeLocator;
    }

    @Override
    public void run() throws Ap3Exception
    {
        Path projectRoot = Paths.get("");

        if (!checkProject(projectRoot))
        {
            return;
        }

        Path containerJar = null;
        String containerVersion = version;

        try
        {
            if ("LATEST".equals(version))
            {
                containerJar = containerResolver.getLatestContainerJar(forceUpdate);
                containerVersion = containerResolver.getLatestContainerVersion();
            }
            else
            {
                containerJar = containerResolver.getContainerJar(version);
            }

            if (null == containerJar || Files.notExists(containerJar))
            {
                throw new Ap3Exception("Couldn't find a P3 container jar to start!");
            }
        }
        catch (IOException e)
        {
            throw new Ap3Exception("Error downloading container", e);
        }

        Pair<Path,Path> pluginPackage = packageProject(projectRoot, containerJar, containerVersion);
        Path pluginJar = pluginPackage.getLeft();

        boolean doStart = true;
        
        if (null == pluginJar)
        {
            doStart = prompter.promptForBoolean("Could not package/find a project jar.\nWould you like to start the container without a project plugin installed?");
        }
                
        if(!doStart)
        {
            return;
        }
        
        List<String> commandLine = getCommands(projectRoot, containerJar, pluginPackage);
        try
        {
            ProcessBuilder processBuilder = new ProcessBuilder(commandLine);
            Path containerDir = createContainerTempDir();
            
            System.out.println("Starting P3 Container version " + containerVersion);
            System.out.println("Container working dir is: " + containerDir.toAbsolutePath().toString());
            System.out.println("running p3 with: " + StringUtils.join(commandLine," "));

            // execute the process in the specified buildRoot
            processBuilder.directory(containerDir.toFile());
            processBuilder.redirectErrorStream(true);

            exportOAuth(projectRoot,processBuilder);
            
            final Process process = processBuilder.start();
            new Thread(new Runnable()
            {
                public void run()
                {
                    try
                    {
                        IOUtils.copy(process.getInputStream(), System.out);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }).start();

            process.waitFor();
        }
        catch (InterruptedException | IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private Path createContainerTempDir() throws IOException
    {
        SecureRandom random = new SecureRandom();
        long n = random.nextLong();
        n = (n == Long.MIN_VALUE) ? 0 : Math.abs(n);
        
        final Path tmpdir = homeLocator.getContainerDirectory().resolve("p3-work").resolve("p3container" + Long.toString(n));
        Files.createDirectories(tmpdir);

        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                try
                {
                    if (tmpdir != null && Files.exists(tmpdir))
                    {
                        System.out.println("removing tmp dir: " + tmpdir.toAbsolutePath().toString());
                        NIOFileUtils.cleanDirectory(tmpdir);

                        Files.deleteIfExists(tmpdir);
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
        
        return tmpdir;
    }

    private void exportOAuth(Path projectRoot, ProcessBuilder processBuilder) throws IOException
    {
        OAuthConfigurationEntry oauthConfig = configurationManager.getProjectConfigurationEntry(OAuthConfigurationEntry.class,projectRoot);
        if(null != oauthConfig)
        {
            String pvtKey = oauthConfig.getPvtkey();
            
            if(StringUtils.isNotBlank(pvtKey))
            {
                processBuilder.environment().put("OAUTH_LOCAL_PRIVATE_KEY", pvtKey);
            }
        }
    }

    private List<String> getCommands(Path projectRoot, Path containerJar, Pair<Path,Path> pluginPackage) throws Ap3Exception
    {
        List<String> args = getJavaArgs(projectRoot,pluginPackage.getRight());
        Path pluginJar = pluginPackage.getLeft();
        
        String javacmd = System.getProperty("ap3.javacmd");
        if(StringUtils.isBlank(javacmd))
        {
            javacmd = "java";
        }
        
        ImmutableList.Builder<String> commandsBuilder = ImmutableList.builder();
        commandsBuilder.add(javacmd)
                .addAll(args)
                .add("-jar")
                .add(containerJar.toAbsolutePath().toString());
        
        if(null != pluginJar && Files.exists(pluginJar))
        {
            commandsBuilder.add(pluginJar.toAbsolutePath().toString());
        }
        
        addContainerArgs(commandsBuilder);

        return commandsBuilder.build();
    }

    private Pair<Path,Path> packageProject(Path projectRoot, Path containerJar, String containerVersion) throws Ap3Exception
    {
        Path pluginJar = null;
        ProjectPackager packager = null;
        KitConfigurationEntry kitEntry = configurationManager.getProjectConfigurationEntry(KitConfigurationEntry.class, projectRoot);

        if (null != kitEntry)
        {
            packager = packagerLocator.getPackager(kitEntry.getId());
        }

        if (null == packager)
        {
            prompter.showError("Could not find a project packager compatible with this project, aborting...");
            return null;
        }

        try
        {
            pluginJar = packager.packageProject(projectRoot, containerJar, containerVersion, getExtraJvmArgs());
        }
        catch (IOException e)
        {
            throw new Ap3Exception("Error packaging project", e);
        }

        return Pair.of(pluginJar,packager.getResourcesPath(projectRoot));
    }

    private boolean checkProject(Path projectRoot) throws PrompterException
    {
        Path projectFile = projectRoot.resolve(Ap3ConfigurationManager.PROJECT_CONFIG_FILENAME);
        if (Files.notExists(projectFile))
        {
            prompter.showError("Current directory is not a valid AP3 project.");
            prompter.showWarning("Either re-run this command with the -g|--global flag, or run: ap3 init");
            return false;
        }

        return true;
    }

    private void addContainerArgs(ImmutableList.Builder<String> commandsBuilder)
    {
        if (port > 0)
        {
            commandsBuilder.add("-p").add(port.toString());
        }

        String verbosity = "";
        if(verbosity1)
        {
            verbosity = "-v";
        }
        if(verbosity2)
        {
            verbosity = "-vv";
        }
        if(verbosity3)
        {
            verbosity = "-vv";
        }
        
        if(StringUtils.isNotBlank(verbosity))
        {
            commandsBuilder.add(verbosity);
        }
    }

    private List<String> getJavaArgs(Path projectRoot, Path resourceDir)
    {
        ImmutableList.Builder<String> argsBuilder = ImmutableList.builder();

        Path dumpFile = Paths.get(System.getProperty("java.io.tmpdir")).resolve("p3container.hprof");
        try
        {
            Files.deleteIfExists(dumpFile);
        }
        catch (IOException e)
        {
            //just ignore it
        }

        if (debug)
        {
            if (debugPort < 1)
            {
                debugPort = 5004;
            }

            String suspend = "n";
            if (debugSuspend)
            {
                suspend = "y";
            }

            argsBuilder.add("-Xdebug")
                       .add("-Xrunjdwp:transport=dt_socket,server=y,suspend=" + suspend + ",address=" + debugPort);
        }

        argsBuilder.add("-Xmx512m")
                   .add("-XX:MaxPermSize=256m")
                   .add("-XX:+HeapDumpOnOutOfMemoryError")
                   .add("-XX:HeapDumpPath=" + dumpFile.toAbsolutePath().toString())
                   .add("-Dplugin.resource.directories=" + resourceDir.toAbsolutePath().toString())
                   .add("-Datlassian.dev.mode=true");
        
        return JVMArgsUtil.mergeToList(argsBuilder.build(),getExtraJvmArgs());
    }
}
