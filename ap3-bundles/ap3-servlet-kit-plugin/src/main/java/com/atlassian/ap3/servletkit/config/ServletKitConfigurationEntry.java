package com.atlassian.ap3.servletkit.config;

import com.atlassian.ap3.api.annotation.ConfigurationEntry;

@ConfigurationEntry("servlet-kit")
public class ServletKitConfigurationEntry
{
    private String buildCommand;

    public String getBuildCommand()
    {
        return buildCommand;
    }

    public void setBuildCommand(String buildCommand)
    {
        this.buildCommand = buildCommand;
    }
}
