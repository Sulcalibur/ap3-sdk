package com.atlassian.ap3.home;

import java.io.IOException;
import java.nio.file.Path;

import com.google.inject.ImplementedBy;

@ImplementedBy(HomeLocatorImpl.class)
public interface HomeLocator
{
    public static final String AP3_HOME_PROP = "AP3_HOME";
    public static final String AP3_LIB_DIR = "lib";
    public static final String AP3_PLUGINS_DIR = "plugins";
    public static final String AP3_CONFIG_DIR = ".ap3";
    public static final String AP3_TEMPLATE_DIR = "templates";
    public static final String AP3_CONTAINER_DIR = "container";
    
    Path getHomeDirectory() throws IOException;
    Path getPluginsDirectory() throws IOException;
    Path getLibDirectory() throws IOException;
    Path getConfigDirectory() throws IOException;
    Path getConfigTemplateDirectory() throws IOException;
    Path getContainerDirectory() throws IOException;
    Path getUserHomeDirectory() throws IOException;
}
